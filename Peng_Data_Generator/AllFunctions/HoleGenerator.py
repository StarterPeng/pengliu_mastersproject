from scipy import spatial
import numpy as np
from Peng_Data_Generator.AllFunctions.CommonFunctions import saveAsOBJ
class HoleGenerators():

    def makeHoles(self, pointList, maxHoleCount, maxHoleSize,holeshapstype):
        # get the shape of hole (0.random,1.circle, 2.square ,3.rectangle)
        #  = 0
            if maxHoleCount > 0:
                # Get total number of points
                pointCnt = pointList.shape[0]
                fixedPoints=np.copy(pointList)
                # Randomly from #pointCnt to pick # maxholeCount
                startIndices = np.random.choice(pointCnt, maxHoleCount, replace=False)
                startPoints = pointList[startIndices]
                # print("startPoints:{}".format(startPoints))
                # print("startPoints_shape:{}".format(startPoints.shape))

                # Make KDTree(K-Dimentional Tree)
                tree = spatial.KDTree(pointList)

                # For each starting point...
                allPointsToRemove = []
                for start in startPoints:
                    # get seeds Coords
                    # print("dadadssssssasdasd")
                    startCoordeinates = list(start)
                    startX = startCoordeinates[0]
                    startY = startCoordeinates[1]
                    startZ = startCoordeinates[2]

                    # get the shape of hole (1.shpere, 2.square ,3.rectangle)
                    if (holeshapstype == 0):
                        holeshape = np.random.randint(low=1, high=4)
                    else:
                        holeshape = holeshapstype

                    # Get size of hole
                    # numpy.random.uniform(low,high,size)
                    # when size is default, return one value.
                    # holeSize = np.random.uniform(low=1, high=maxHoleSize)
                    holeSize = maxHoleSize
                    # if maxHoleSize<20:
                    #     maxHoleSize=20
                    rectangleSize = np.random.uniform(low=25, high=holeSize)

                    # whether square or rectangle
                    corpRateX = 1
                    corpRateY = 1
                    corpRateZ = 1
                    if (holeshape == 2):
                        corpRateX = 1.5
                        corpRateY = 1.5
                        corpRateZ = 1.5
                    if (holeshape == 3):
                        # corpRateX = np.random.uniform(low=1, high=3)
                        # corpRateY = np.random.uniform(low=1, high=corpRateX)
                        # corpRateZ = np.random.uniform(low=1, high=corpRateX)
                        corpRateX = 1.5
                        corpRateY = 2
                        corpRateZ = 2

                    # print("HoleShape"+holeshape)

                    intervalX = []
                    intervalX.append(startX - rectangleSize / corpRateX)
                    intervalX.append(startX + rectangleSize / corpRateX)

                    intervalY = []
                    intervalY.append(startY - rectangleSize / corpRateY)
                    intervalY.append(startY + rectangleSize / corpRateY)

                    intervalZ = []
                    intervalZ.append(startZ - rectangleSize / corpRateZ)
                    intervalZ.append(startZ + rectangleSize / corpRateZ)

                    # Get indices of nearest neighbors,holesize is distance
                    # scipy.spatial.KDTree.query_ball_tree(x, r, p=2.0, eps=0)
                    holeNeighbors = tree.query_ball_point(start, holeSize)

                    #
                    if (holeshape >= 2):
                        holeNeighbors_2 = []
                        for n in range(pointCnt):
                            # i in (pointCnt-allPointsToRemove)
                            if n in holeNeighbors:
                                temppoint = pointList[n]
                                temppointCoordinate = list(temppoint)
                                tempx = temppointCoordinate[0]
                                tempy = temppointCoordinate[1]
                                tempz = temppointCoordinate[2]
                                # if( x y are in interval x, y ??)
                                if (tempx > intervalX[0] and tempx < intervalX[1] and tempy > intervalY[0] and tempy <
                                        intervalY[1]
                                        and tempz > intervalZ[0] and tempz < intervalZ[1]):
                                    holeNeighbors_2.append(n)

                        allPointsToRemove += holeNeighbors_2

                    else:
                        allPointsToRemove += holeNeighbors

                allPointsToRemove = list(set(allPointsToRemove))
                # bounds.
                # print(allPointsToRemove)
                # print(allPointsToRemove)

                # Get points that are NOT in the removal set
                # Set "0"
                newPoints = np.zeros_like(pointList)
                index = 0
                while index < pointCnt:
                    for i in range(pointCnt):
                        # i in (pointCnt-allPointsToRemove)
                        if i not in allPointsToRemove and index < pointCnt:
                            newPoints[index] = pointList[i]

                            # print(newPoints[index])

                            index += 1

                # maskPoints = np.zeros_like(pointList)
                # allPointsToRemove=np.array([allPointsToRemove])
                # index=0
                # while index < allPointsToRemove.shape[0]:
                #     for i in range(allPointsToRemove.shape[0]):
                #         if i in allPointsToRemove and index<allPointsToRemove.shape[0]:
                #             maskPoints[index]=allPointsToRemove[i]
                #             index+=1
                # saveAsOBJ(maskPoints,"maskpoints.obj")
                return newPoints,allPointsToRemove,fixedPoints

            else:
                return pointList,[],[]

