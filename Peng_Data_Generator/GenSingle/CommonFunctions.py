# MIT LICENSE
#
# Copyright 2018 Ben F. Klinghoffer, Hannah M. Szmurlo, Michael J. Reale, Peng Liu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import absolute_import, print_function
from scipy.spatial import distance_matrix, distance
import random
# import h5py
# import numpy as np
# from scipy import ndimage
# import datetime
# # from Data.ObjLoader import ObjLoader
# # from Data.BP4DFeaturePoints3D import *
# # from Data.Cached3DSequenceData import *
# import math
# #from scipy.spatial import KDTree
# from sklearn.neighbors import KDTree
# from sklearn.neighbors import BallTree
# #from scipy.spatial import cKDTree as KDTree

def extractPointsFromObj(objFile):
    allPoints = []

    f = open(objFile)
    for line in f:
        line = line.split()
        if len(line) <= 0:
            continue
        elif line[0] == 'v':
            temp = [float(point) for point in line[1:4]]
            allPoints.append(temp)

    random.shuffle(allPoints)

    return allPoints




def extractFeaturePoints(tbndFile, startRow=1):
    featurePoints = []
    f = open(tbndFile)
    for line in f:
        line = line.split()
        featurePoints.append([float(x) for x in line])

    return featurePoints[startRow:]



def cropFace(allPoints, featurePoints, avgPointsIndex, chinIndex):
    nosePoints = [featurePoints[i] for i in avgPointsIndex]
    facePoint = featurePoints[chinIndex]
    noseAvg = np.average(nosePoints, axis = 0)
    cropRange = distance.euclidean(noseAvg, facePoint)

    cropedPoints = []

    for point in allPoints:
        if distance.euclidean(noseAvg, point) < cropRange:
            cropedPoints.append(point)

    return cropedPoints[:]


def desiredFeatureIndexToPoints(desiredFeaturePoints, featurePoints):
    desiredPoints = []
    for point in desiredFeaturePoints:
        if isinstance(point, list):
            point = np.average([featurePoints[i] for i in point], axis=0)
        else:
            point = featurePoints[point]
        desiredPoints.append(point)
    return desiredPoints


def getRanPointCloud(allPoints, featurePoints):
    # if params.doFaceCrop:
    allPoints = cropFace(allPoints, featurePoints,avgPointsIndex=[39, 44, 20, 28], chinIndex=75)
    somePoints = allPoints[:4096]
    while len(somePoints) < 4096:
        # print("Cloud too small (", len(somePoints), "points ), reusing data points...")
        correctionNumber = 4096 - len(somePoints)
        somePoints += somePoints[:correctionNumber]
    # if params.isRelativeToPoints:
        # featurePoints = extractFeaturePoints(tbndFile)
    return somePoints







def poseNormalize(points, featureData):
    # Append 1 to each point
    ones = np.array([[1.0]] * len(points))
    hPoints = np.concatenate([points, ones], axis=1)

    # Get matrices
    modelCenter = featureData.getBackwardCenterTranslateMatrix()
    modelPose = featureData.getBackwardPoseMatrix()
    model = np.matmul(modelPose, modelCenter)

    # Pose correct
    normPoints = np.matmul(hPoints, model)

    # Remove the homogeneous coordinates
    normPoints = normPoints[:, 0:3]

    return normPoints


# DEBUG_alreadySaved = False

def saveAsOBJ(points, filename):
    with open(filename, 'w') as f:
        for i in range(len(points)):
            s = "v "
            for j in range(3):
                s += str(points[i][j]) + " "
            s += "\n"
            f.write(s)

def saveAsOBJPointsAndVerts(points, simplices, filename):
    with open(filename,'w') as f:

            for i in range(len(points)):
                s = "v "
                for j in range(3):
                    s += str(points[i][j]) + " "
                s += "\n"
                f.write(s)

            for k in range(len(simplices)):
                s = "f "
                for g in range(3):
                    s += str(simplices[k][g]+1) + "/"
                s += "\n"
                f.write(s)


def save_UV_and_facets(points, simplices, filename):
    with open(filename,'w') as f:

            for i in range(len(points)):
                s = "v "
                for j in range(2):
                    s += str(points[i][j]) + " "
                s+=str(0)
                s += "\n"
                f.write(s)

            for k in range(len(simplices)):
                s = "f "
                for g in range(3):
                    s += str(simplices[k][g]) + " "
                s += "\n"
                f.write(s)

def save_UVZ_and_facets(points, simplices, filename):
    with open(filename,'w') as f:

            for i in range(len(points)):
                s = "v "
                for j in range(3):
                    s += str(points[i][j]) + " "
                # s+=str(0)
                s += "\n"
                f.write(s)

            for k in range(len(simplices)):
                s = "f "
                for g in range(4):
                    s += str(simplices[k][g]+1) + " "
                s += "\n"
                f.write(s)
def save_UVZ(points, filename):
    with open(filename,'w') as f:

            for i in range(len(points)):
                s = "v "
                for j in range(3):
                    s += str(points[i][j]) + " "
                # s+=str(0)
                s += "\n"
                f.write(s)




def isPointInTri(point, tri_points):
    #    Title: face3d
    #    Author: Yao Feng
    #    Date: 2018
    #    Code version: unknown
    #    Availability: https://github.com/YadiraF/face3d

    ''' Judge whether the point is in the triangle
    Method:
        http://blackpawn.com/texts/pointinpoly/
    Args:
        point: (2,). [u, v] or [x, y]
        tri_points: (3 vertices, 2 coords). three vertices(2d points) of a triangle.
    Returns:
        bool: true for in triangle
    '''
    tp = tri_points

    # vectors
    v0 = tp[2,:] - tp[0,:]
    v1 = tp[1,:] - tp[0,:]
    v2 = point - tp[0,:]

    # dot products
    dot00 = np.dot(v0.T, v0)
    dot01 = np.dot(v0.T, v1)
    dot02 = np.dot(v0.T, v2)
    dot11 = np.dot(v1.T, v1)
    dot12 = np.dot(v1.T, v2)

    # barycentric coordinates
    if dot00*dot11 - dot01*dot01 == 0:
        inverDeno = 0
    else:
        inverDeno = 1/(dot00*dot11 - dot01*dot01)

    u = (dot11*dot02 - dot01*dot12)*inverDeno
    v = (dot00*dot12 - dot01*dot02)*inverDeno

    # check if point in triangle
    return (u >= 0) & (v >= 0) & (u + v < 1)


def get_point_weight(point, tri_points):
    ''' Get the weights of the position
    #    Title: face3d
    #    Author: Yao Feng
    #    Date: 2018
    #    Code version: unknown
    #    Availability: https://github.com/YadiraF/face3d

    Methods: https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
     -m1.compute the area of the triangles formed by embedding the point P inside the triangle
     -m2.Christer Ericson's book "Real-Time Collision Detection". faster.(used)
    Args:
        point: (2,). [u, v] or [x, y]
        tri_points: (3 vertices, 2 coords). three vertices(2d points) of a triangle.
    Returns:
        w0: weight of v0
        w1: weight of v1
        w2: weight of v3
     '''
    tp = tri_points
    # print("tri_points:{}".format(tri_points))
    # print("point:{}".format(point))
    # vectors
    v0 = tp[2,:] - tp[0,:]
    v1 = tp[1,:] - tp[0,:]
    v2 = point - tp[0,:]

    # dot products
    dot00 = np.dot(v0.T, v0)
    dot01 = np.dot(v0.T, v1)
    dot02 = np.dot(v0.T, v2)
    dot11 = np.dot(v1.T, v1)
    dot12 = np.dot(v1.T, v2)

    # barycentric coordinates
    if dot00*dot11 - dot01*dot01 == 0:
        inverDeno = 0
    else:
        inverDeno = 1/(dot00*dot11 - dot01*dot01)

    u = (dot11*dot02 - dot01*dot12)*inverDeno
    v = (dot00*dot12 - dot01*dot02)*inverDeno

    w0 = 1 - u - v
    w1 = v
    w2 = u

    return w0, w1, w2



def render_mask_colors(vertices, triangles, colors, h, w, c=3):
    #    Title: face3d
    #    Author: Yao Feng
    #    Date: 2018
    #    Code version: unknown
    #    Availability: https://github.com/YadiraF/face3d
    assert vertices.shape[0] == colors.shape[0]
    # initial
    image = np.zeros((h, w, c))
    depth_buffer = np.zeros([h, w]) - 999999.

    for i in range(triangles.shape[0]):
        tri = triangles[i, :]  # 3 vertex indices
        # the inner bounding box
        umin = max(int(np.ceil(np.min(vertices[tri, 0]))), 0)
        umax = min(int(np.floor(np.max(vertices[tri, 0]))), w - 1)
        # print("verticesmin[tri, 0]:{}".format(np.min(vertices[tri, 0])))
        vmin = max(int(np.ceil(np.min(vertices[tri, 1]))), 0)
        vmax = min(int(np.floor(np.max(vertices[tri, 1]))), h - 1)

        if umax < umin or vmax < vmin:
            continue

        for u in range(umin, umax + 1):
            for v in range(vmin, vmax + 1):
                if not isPointInTri([u, v], vertices[tri, :2]):
                    continue
                w0, w1, w2 = get_point_weight([u, v], vertices[tri, :2])
                # print([w0, w1, w2])
                # TODO figure out what happend here, point_depth always be '0', although w0 w1 w2 are not '0'
                point_depth = w0 * vertices[tri[0], 2] + w1 * vertices[tri[1], 2] + w2 * vertices[tri[2], 2]
                # print("vertices[tri[0], 1]:{}".format(vertices[tri[0], 2]))

                if point_depth > depth_buffer[v, u]:
                    depth_buffer[v, u] = point_depth
                    image[v, u, :] = w0 * colors[tri[0], :] + w1 * colors[tri[1], :] + w2 * colors[tri[2], :]

                    if any(image[v, u, :])>0:
                        image[v, u, :]=[255,255,255]
                    else:
                        image[v, u, :]=[0,0,0]
    return image



def render_mask_bicolor(vertices, triangles, colors, h, w, c=3):
    #    Title: face3d
    #    Author: Yao Feng
    #    Date: 2018
    #    Code version: unknown
    #    Availability: https://github.com/YadiraF/face3d
    assert vertices.shape[0] == colors.shape[0]
    # initial
    bicolor_image = np.zeros((h, w, c))
    depth_buffer = np.zeros([h, w]) - 999999.

    for i in range(triangles.shape[0]):
        tri = triangles[i, :]  # 3 vertex indices
        # the inner bounding box
        umin = max(int(np.ceil(np.min(vertices[tri, 0]))), 0)
        umax = min(int(np.floor(np.max(vertices[tri, 0]))), w - 1)
        # print("verticesmin[tri, 0]:{}".format(np.min(vertices[tri, 0])))
        vmin = max(int(np.ceil(np.min(vertices[tri, 1]))), 0)
        vmax = min(int(np.floor(np.max(vertices[tri, 1]))), h - 1)

        if umax < umin or vmax < vmin:
            continue

        for u in range(umin, umax + 1):
            for v in range(vmin, vmax + 1):
                if not isPointInTri([u, v], vertices[tri, :2]):
                    continue
                w0, w1, w2 = get_point_weight([u, v], vertices[tri, :2])
                # print([w0, w1, w2])
                # TODO figure out what happend here, point_depth always be '0', although w0 w1 w2 are not '0'
                point_depth = w0 * vertices[tri[0], 2] + w1 * vertices[tri[1], 2] + w2 * vertices[tri[2], 2]
                # print("vertices[tri[0], 1]:{}".format(vertices[tri[0], 2]))

                if point_depth > depth_buffer[v, u]:
                    depth_buffer[v, u] = point_depth
                    bicolor_image[v, u, :] = w0 * colors[tri[0], :] + w1 * colors[tri[1], :] + w2 * colors[tri[2], :]

                    # if any(image[v, u, :]) > 0:
                    #     image[v, u, :] = [255, 255, 255]
                    # else:
                    #     image[v, u, :] = [0, 0, 0]

                    if any(bicolor_image[v, u, :])>0:
                        bicolor_image[v, u, :]=[1,1,1]
                    else:
                        bicolor_image[v, u, :]=[0,0,0]
    return bicolor_image

def render_colors(vertices, triangles, colors, h, w, c=3):
    ''' render mesh with colors
    #    Title: face3d
    #    Author: Yao Feng
    #    Date: 2018
    #    Code version: unknown
    #    Availability: https://github.com/YadiraF/face3d

    Args:
        vertices: [nver, 3]
        triangles: [ntri, 3]
        colors: [nver, 3]
        h: height
        w: width
    Returns:
        image: [h, w, c].
    '''
    assert vertices.shape[0] == colors.shape[0]
    # initial
    image = np.zeros((h, w, c))
    depth_buffer = np.zeros([h, w]) - 999999.

    for i in range(triangles.shape[0]):
        #for every tris
        tri = triangles[i, :]

        umin = max(int(np.ceil(np.min(vertices[tri, 0]))), 0)
        umax = min(int(np.floor(np.max(vertices[tri, 0]))), w - 1)
        vmin = max(int(np.ceil(np.min(vertices[tri, 1]))), 0)
        vmax = min(int(np.floor(np.max(vertices[tri, 1]))), h - 1)

        if umax < umin or vmax < vmin:
            continue

        for u in range(umin, umax + 1):
            for v in range(vmin, vmax + 1):
                if not isPointInTri([u,v], vertices[tri, :2]):
                    continue
                w0, w1, w2 = get_point_weight([u,v], vertices[tri, :2])

                point_depth = w0 * vertices[tri[0],2] + w1 * vertices[tri[1],2] + w2 * vertices[tri[2],2]

                if point_depth > depth_buffer[v, u]:
                    depth_buffer[v, u] = point_depth
                    image[v, u, :] = w0 * colors[tri[0], :] + w1 * colors[tri[1], :] + w2 * colors[tri[2], :]

                    ##red
                    # maxR=image[..., 0].max()
                    # maxRAbs=abs(maxR)
                    # minR=image[..., 0].min()
                    # minRAbs=abs(minR)
                    # maxAbsRed=max(maxRAbs,minRAbs)
                    # print("R_Abs_max:{}".format(maxAbsRed))
                    #
                    # ## green
                    # maxG = image[..., 1].max()
                    # maxGAbs = abs(maxG)
                    # minG = image[..., 1].min()
                    # minGAbs = abs(minG)
                    # maxAbsGreen = max(maxGAbs, minGAbs)
                    # print("G_Abs_max:{}".format(maxAbsGreen))
                    # ## blue
                    # maxB = image[..., 2].max()
                    # maxBAbs = abs(maxB)
                    # minB = image[..., 2].min()
                    # minBAbs = abs(minB)
                    # maxAbsBlue = max(maxBAbs, minBAbs)
                    # print("B_Abs_max:{}".format(maxAbsBlue))
                    #
                    # MAXXXX=max(maxAbsBlue,maxAbsGreen,maxAbsRed)
                    # if MAXXXX>156:
                    #     print("YOU ARE WRONG!!!!!!!!!!")
                    #     break

                    # print("R:{}".format((image[..., 0].max())-(image[..., 0].min())))
                    #
                    # print("G:{}".format((image[..., 1].max())-(image[..., 1].min())))
                    #
                    # print("B_range:{}".format((image[..., 2].max())-(image[..., 2].min())))
    return image



def process_uv(uv_coords, uv_h, uv_w):
    uv_coords[:, 0] = uv_coords[:, 0] * (uv_w )
    uv_coords[:, 1] = uv_coords[:, 1] * (uv_h )
    uv_coords[:, 1] = uv_h - uv_coords[:, 1]
    uvCoords = np.hstack((uv_coords, np.zeros((uv_coords.shape[0], 1))))
    return uvCoords

def to_image(vertices, h, w, is_perspective = False):
    ''' change vertices to image coord system
    #    Title: face3d
    #    Author: Yao Feng
    #    Date: 2018
    #    Code version: unknown
    #    Availability: https://github.com/YadiraF/face3d
    3d system: XYZ, center(0, 0, 0)
    2d image: x(u), y(v). center(w/2, h/2), flip y-axis.
    Args:
        vertices: [nver, 3]
        h: height of the rendering
        w : width of the rendering
    Returns:
        projected_vertices: [nver, 3]
    '''
    image_vertices = vertices.copy()
    if is_perspective:
        # if perspective, the projected vertices are normalized to [-1, 1]. so change it to image size first.
        image_vertices[:,0] = image_vertices[:,0]*w/2
        image_vertices[:,1] = image_vertices[:,1]*h/2
    # move to center of image
    image_vertices[:,0] = image_vertices[:,0] + w/2
    image_vertices[:,1] = image_vertices[:,1] + h/2
    # flip vertices along y-axis.
    image_vertices[:,1] = h - image_vertices[:,1] - 1
    return image_vertices
def obj_tris_loader(filename):
    f = open(filename)
    tris = []
    for line in f:
        line = line.split()
        if len(line) <= 0:
            pass
        elif line[0] == 'f':
            tris.append(line[1:])
    return np.array(tris,dtype='int_')

import os
import numpy as np

def obj_firstpath_looper(basepathe):
    F_sample_list=np.array(['F001','F002','F003','F004','F005','M001','M002','M003','M004','M005'])
    F_name_list=np.array([])
    F_path_list = np.array([])
    indexF=0
    for F in os.listdir(basepathe):
        if F in F_sample_list:
            F = F
        # if len(F)==4:
        #     F=F
        else:
            continue
        F_path = os.path.join(basepathe, F)
        F_name_list = np.append(F_name_list, F)
        F_path_list = np.append(F_path_list, F_path)
        indexF+=1
    return F_name_list, F_path_list, indexF

def obj_basepath_looper(basepathe):
    T_name_list = np.array([])
    T_path_list = np.array([])
    index_T = -1
    F_name_list, F_path_list, indexF=obj_firstpath_looper(basepathe)

    #F001~M002
    for F in F_path_list:
        print("basepath:{}".format(F))
        #T1~T8
        for T in os.listdir(F):
            #[../T1~, ..., ../T8]
            T_path=os.path.join(F,T)
            #[T1, ..., T8]
            T_name_list=np.append(T_name_list,T)
            T_path_list=np.append(T_path_list,T_path)
            index_T+=1

    return T_name_list,T_path_list,index_T

def objfile_path_looper(basepath):
    objfile_name_list = np.array([])
    objfile_path_list = np.array([])
    index_obj=-1
    T_name_list, T_path_list, index_T=obj_basepath_looper(basepath)
    print("T_name_list:{}".format(T_name_list))
    print("T_path_list:{}".format(T_path_list))
    # for T in range(index_T):
    for T_path in T_path_list:
        #[objs in T(i)]
        # for objfile in os.listdir(T_path_list[i]):
        for objfile in os.listdir(T_path):
            if objfile[-3: ]=='obj':
                objfile=objfile
            #     continue
            else:
                continue
            # objfile_path=os.path.join(T_path_list[i],objfile)
            objfile_path = os.path.join(T_path, objfile)
            # [0000.obj, ..., ijgk.obj]
            objfile_name_list=np.append(objfile_name_list,objfile)
            # [../0000.obj, ..., ../ijgk.obj]
            objfile_path_list=np.append(objfile_path_list,objfile_path)
            index_obj+=1
        objfile_name_list=objfile_name_list
        objfile_path_list=objfile_path_list
    print ("objfile_path_list:{}".format(objfile_path_list))
    return objfile_name_list,objfile_path_list,index_T,index_obj

def tbnd_firstpath_looper(basepathe):
    F_sample_list=np.array(['F001','F002','F003','F004','F005','M001','M002','M003','M004','M005'])
    F_name_list = np.array([])
    F_path_list = np.array([])
    indexF = 0
    for F in os.listdir(basepathe):
        if F in F_sample_list:
            F=F
        # if len(F)==4:
        #     F=F
        else:
            continue
        F_path = os.path.join(basepathe, F)
        F_name_list = np.append(F_name_list, F)
        F_path_list = np.append(F_path_list, F_path)
        indexF += 1
    return F_name_list, F_path_list, indexF


def tbnd_basepath_looper(basepathes):
    T_name_list = np.array([])
    T_path_list = np.array([])
    index_T = -1
    F_name_list, F_path_list, indexF=tbnd_firstpath_looper(basepathes)
    # F001~M002
    for F in F_path_list:
        print("basepath:{}".format(F))
        # T1~T8
        for T in os.listdir(F):
            # [../T1~, ..., ../T8]
            T_path = os.path.join(F, T)
            # [T1, ..., T8]
            T_name_list = np.append(T_name_list, T)
            T_path_list = np.append(T_path_list, T_path)
            index_T += 1

    return T_name_list, T_path_list, index_T


def tbndfile_path_looper(basepath):
    tbndfile_name_list = np.array([])
    tbndfile_path_list = np.array([])
    index_tbnd=-1
    T_name_list, T_path_list, index_T=obj_basepath_looper(basepath)
    print("T_name_list:{}".format(T_name_list))
    print("T_path_list:{}".format(T_path_list))
    # for T in range(index_T):
    for T_path in T_path_list:

        for tbndfile in os.listdir(T_path):
            if tbndfile[-4: ]=='tbnd':
                tbndfile=tbndfile
            #     continue
            else:
                continue
            tbndfile_path = os.path.join(T_path, tbndfile)
            # [0000.obj, ..., ijgk.obj]
            tbndfile_name_list=np.append(tbndfile_name_list,tbndfile)
            # [../0000.obj, ..., ../ijgk.obj]
            tbndfile_path_list=np.append(tbndfile_path_list,tbndfile_path)
            index_tbnd+=1

        tbndfile_name_list=tbndfile_name_list
        tbndfile_path_list=tbndfile_path_list
    print ("tbndfile_path_list:{}".format(tbndfile_path_list))
    return tbndfile_name_list,tbndfile_path_list,index_T,index_tbnd
