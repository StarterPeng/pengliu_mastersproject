import os
from os import path
from skimage import io
from PIL import Image
import numpy as np

img = Image.open("/home/liup/Desktop/imagesForshow/0000mask.png", mode="r")
width = img.size[0]
height = img.size[1]

for x in range(width):
    for y in range(height):
        r, g, b = img.getpixel((x, y))
        if (r > 0):
            img.putpixel((x, y), (1, 1, 1))
        else:
            img.putpixel((x, y), (0, 0, 0))

img = np.array(img)
io.imsave('{}.png'.format("/home/liup/Desktop/imagesForshow/0000bicolor"), img)