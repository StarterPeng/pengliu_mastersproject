import os
import numpy as np
from skimage import io
from Peng_Data_Generator.GenSingle.UVGeneratorNew import UvGenerator
from Peng_Data_Generator.GenSingle.CommonFunctions import process_uv, render_colors,render_mask_colors



def myFunc(obj_path, tbnd_path, full_uv_save_folder,uv_hole_save_folder,mask_save_folder):
    uv_map = UvGenerator().gen_uv_space(objFile=obj_path, tbndFile=tbnd_path, maxHoleCount=3, maxHoleSize=20,
                                        holeshapstype=0)


    # ----load mesh data, & UV space coords----
    holy_uv_array, holy_tris, holycloud, mask_uv_array, mask_tris, mask_color, full_uv_array, full_tris, coords = uv_map

    # ---width,height---
    # UV enlarging
    uv_h = uv_w = 64

    # ---render color for full uv without holes
    # starttime = time.time()
    full_uv_array = process_uv(full_uv_array, uv_h, uv_w)
    full_uv_position_map = render_colors(full_uv_array, full_tris, coords, uv_h, uv_w, c=3)


    mask_uv_array = process_uv(mask_uv_array, uv_h, uv_w)
    uv_mask_map = render_mask_colors(mask_uv_array, mask_tris, mask_color, uv_h, uv_w, c=3)

    holy_uv_array = process_uv(holy_uv_array, uv_h, uv_w)
    uv_position_map = render_colors(holy_uv_array, holy_tris, holycloud, uv_h, uv_w, c=3)



    path, filename = os.path.split(obj_path)
    list = path.split("/")

    uv_p = list[-2:]
    uv_p_subject = uv_p[0]
    uv_p_task = uv_p[1]
    uv_path = '/' + uv_p_subject + '/' + uv_p_task+'/'
    number = filename[:-4]


    imageRescale_high_contrast = 120 #90
    maskimageRescale=255
    # DEBUG
    # print("Type of array:", type(full_uv_position_map))
    # print("More type data:", full_uv_position_map.shape)
    # print("Type of element:", full_uv_position_map.dtype)
    # print("Max value:", full_uv_position_map.max())
    # print("Min value:", full_uv_position_map.min())
    # END DEBUG

    #save
    #save full uv image without holes

    #save uv


    # # save full UV images
    minVal = full_uv_position_map.min()
    maxVal = full_uv_position_map.max()
    if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
        print("WARNING: Data does not fit to range:", minVal, maxVal)

    full_uv_position_map = np.minimum(full_uv_position_map, imageRescale_high_contrast)
    full_uv_position_map = np.maximum(full_uv_position_map, -imageRescale_high_contrast)

    if os.path.exists(full_uv_save_folder + uv_path):
        pass
    else:
        os.makedirs(full_uv_save_folder + uv_path, mode=0o777)
    image_save_path = full_uv_save_folder + uv_path
    io.imsave('{}.png'.format(image_save_path + number), (full_uv_position_map)/imageRescale_high_contrast)

    #save hole uv
    minVal = uv_position_map.min()
    maxVal = uv_position_map.max()
    if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
        print("WARNING: Data does not fit to range:", minVal, maxVal)

    uv_position_map = np.minimum(uv_position_map, imageRescale_high_contrast)
    uv_position_map = np.maximum(uv_position_map, -imageRescale_high_contrast)

    if os.path.exists(uv_hole_save_folder + uv_path):
        pass
    else:
        os.makedirs(uv_hole_save_folder + uv_path, mode=0o777)
    image_save_path = uv_hole_save_folder + uv_path
    io.imsave('{}.png'.format(image_save_path + number), (uv_position_map)/imageRescale_high_contrast)



    #save UV masks
    if os.path.exists(mask_save_folder + uv_path):
        pass
    else:
        os.makedirs(mask_save_folder + uv_path, mode=0o777)
    image_save_path = mask_save_folder + uv_path
    io.imsave('{}.png'.format(image_save_path + number), (uv_mask_map)/maskimageRescale)

myFunc('/mnt/Core/BP4D_Peng_Nov_9/singleGenNov17/OBJ/F888/T8/0000.obj',
       '/mnt/Core/BP4D_Peng_Nov_9/singleGenNov17/TBND/F888/T8/0000.tbnd',
       '/mnt/Core/BP4D_Peng_Nov_9/singleGenNov17/Full_uv',
       '/mnt/Core/BP4D_Peng_Nov_9/singleGenNov17/Hole_uv',
       '/mnt/Core/BP4D_Peng_Nov_9/singleGenNov17/Mask_uv')