import numpy as np
import math
import os, sys
from skimage import io
from scipy.spatial import Delaunay
from Peng_Data_Generator.GenSingle.HoleGenerator import HoleGenerators
from Peng_Data_Generator.GenSingle.BP4DFeaturePoints3D import BP4DFeaturePoints3D
from Peng_Data_Generator.GenSingle.CommonFunctions import poseNormalize, \
    getRanPointCloud, \
    extractPointsFromObj, \
    extractFeaturePoints,saveAsOBJ,save_UV_and_facets,save_UVZ_and_facets,save_UVZ
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


class UvGenerator():
    def __init__(self):
        self.Gen_hole = HoleGenerators()

    def norm_point_clouds(self, objFile, tbndFile):
        # Do pose normalization and crop
        allPoints = extractPointsFromObj(objFile)
        featurePoints = extractFeaturePoints(tbndFile)

        if isinstance(featurePoints[0], int):
            featurePoints = [allPoints[index] for index in featurePoints]
        else:
            pass
        featureData = BP4DFeaturePoints3D(tbndFile)
        allPoints = poseNormalize(allPoints, featureData)
        featurePoints = poseNormalize(featurePoints, featureData)
        pointCloud = getRanPointCloud(allPoints, featurePoints)

        return np.array(pointCloud)

    def gen_holes(self, objFile, tbndFile, maxHoleCount, maxHoleSize, holeshapstype):
        pointCloud = self.norm_point_clouds(objFile, tbndFile)
        # get the shape of hole (0.random,1.circle, 2.square ,3.rectangle)
        holypointCloud, holePoints, fixedpoints = self.Gen_hole.makeHoles(pointCloud, maxHoleCount, maxHoleSize,
                                                                      holeshapstype)

        #----------------Edited Nov 3rd-------------------
        #----------------hole position shifted------------
        # print("hole polints shifted(UVgeneratorNew().gen_holes)")
        # holePoints[:,2] = holePoints[:, 2] - np.min(holePoints[:, 2])
        # ----------------Edited Nov 3rd-------------------
        # ----------------hole position shifted , it wont mess up the rescale------------

        fixedpoints[:, 2] = fixedpoints[:, 2] - np.min(fixedpoints[:, 2])
        holypointCloud[:, 2] = holypointCloud[:, 2] - np.min(holypointCloud[:, 2])
        ################################3
        saveAsOBJ(fixedpoints,'0000Full.obj')
        saveAsOBJ(holypointCloud, '0000hole.obj')
###################################################################
        figOBJ = plt.figure(dpi=500)
        ax = figOBJ.add_subplot(111, projection='3d')

        # colors = fixedpoints[:, 3:] / 4096  # RGBA(0-1)
        # intfixedpoints1=fixedpoints
        # intfixedpoints2 = np.trunc(intfixedpoints1)
        # intfixedpoints3 = intfixedpoints2.astype(int)
        # center = np.sum(fixedpoints[:, 0]+fixedpoints[:, 1]+fixedpoints[:, 2], axis=0) / 3.0
        # center=np.trunc(center)
        # center=center.astype('int32')
        # if center.any()<0:
        #     center=center-2*center
        #
        # colors = np.array([(x - 400/ 2) ** 2 + (y - 400/ 2) ** 2 for x, y in center])
        ax.scatter(fixedpoints[:, 0], fixedpoints[:, 1], fixedpoints[:, 2],
                   cmap='spectral',
                   # c=colors,
                   s=0.5,
                   linewidth=0,
                   alpha=1,
                   marker=".")

        plt.title('Point Cloud')
        # ax.axis('scaled')  # {equal, scaled}
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        plt.show()
#############################################################



        return holypointCloud, holePoints, np.array(fixedpoints)

    def gen_uv_space(self, objFile, tbndFile, maxHoleCount, maxHoleSize, holeshapstype):
        '''Cylindrical projection(UV mapping)'''
        holycloud, maskPoints, coords = self.gen_holes(objFile, tbndFile, maxHoleCount, maxHoleSize, holeshapstype)

        # Get uv mapping for hole-ridden data
        holy_uv_array, holy_tris = self.gen_one_uv_space(holycloud)

        # Get MASK uv mapping, use the coord that uv uses, because they are corresponding to each others
        #----------------fixed input---------------------
        # mask_uv_array, mask_tris = self.gen_one_uv_space(coords)
        mask_uv_array, mask_tris = self.gen_one_uv_space(coords)
        # ----------------fixed input---------------------


        # Get uv without holes
        full_uv_array, full_tris = self.gen_one_uv_space(coords)
#######################################################################
        # print("Debug00")
        save_UV_and_facets(holy_uv_array,holy_tris,'0000hole2D0.obj')
        save_UV_and_facets(full_uv_array, full_tris, '0000full2D0.obj')
        # Plot Delaunay triangle with color filled
        center = np.sum(holy_uv_array[holy_tris], axis=1) / 3.0
        color = np.array([(x - 400/ 2) ** 2 + (y - 400/ 2) ** 2 for x, y in center])
        plt.figure(figsize=(3, 3))
        plt.tripcolor(holy_uv_array[:, 0], holy_uv_array[:, 1], holy_tris.copy(), facecolors=color, edgecolors='k')

        # Delete ticks, axis and background
        plt.tick_params(labelbottom='off', labelleft='off', left='off', right='off',
                        bottom='off', top='off')
        ax = plt.gca()
        ax.spines['right'].set_color('none')
        ax.spines['bottom'].set_color('none')
        ax.spines['left'].set_color('none')
        ax.spines['top'].set_color('none')
        # Save picture
        plt.savefig('Delaunay.png', transparent=True, dpi=600)
###############################################################################################



        # Get "color" for mask
        mask_color = []
        for index in range(len(coords)):
            if index in maskPoints:
                # mask_color.append([63, 63, 63])
                mask_color.append([63,63, 63])
            else:
                mask_color.append([0, 0, 0])

        return np.array(holy_uv_array), \
               np.array(holy_tris), \
               np.array(holycloud), \
               np.array(mask_uv_array), \
               np.array(mask_tris), \
               np.array(mask_color), \
               np.array(full_uv_array), \
               np.array(full_tris), \
               np.array(coords)

    def gen_one_uv_space(self, coords):

        # Extracts individual coordinates
        coordX = coords[:, 0]
        coordY = coords[:, 1]
        coordZ = coords[:, 2]

        # Get min and max Y values
        Ymax = np.max(coordY)
        Ymin = np.min(coordY)

        # For each point...
        uv_array = []


        # # ------------------------------------------------nov4
        # uvw_array=[]
        # # ------------------------------------------------nov4


        for i in range(len(coordX)):
            theta = math.atan2(coordZ[i], coordX[i])
            # Value is between [0, pi]
            u_temp = theta

            u = u_temp / (math.pi)
            v = (coordY[i] - Ymin) / (Ymax - Ymin)

            w=0

# #------------------------------------------------nov4
#             coodsuvw=[u,v,w]
# #------------------------------------------------nov4
#             uvw_array.append(coodsuvw)
# #------------------------------------------------nov4
#             trisuvw = Delaunay(uvw_array)
#             trisuvw = trisuvw.simplices
#
#
# # ------------------------------------------------nov4
            coord = [u, v]
            uv_array.append(coord)
            # print('min and max:{}'.format(min(uv_array),max(uv_array)))

        uv_array = np.array(uv_array)
        # Get triangulation
        tris = Delaunay(uv_array)
        tris = tris.simplices


        return uv_array, tris







        # def gen_uv_map(self):

# ## --generate uv space(with or without holes)
# RunUvGen=UvGenerator().gen_uv_space(objFile="0000.obj", tbndFile="0000.tbnd", maxHoleCount=0, maxHoleSize=20, holeshapstype=0)
#
# ## --load mesh data, & UV space coords
# uv_coords,triangles,vertices=RunUvGen
# vertices[:,2] = vertices[:,2] - np.min(vertices[:,2]) # translate z
#
# # -- start
# save_folder = 'output'
# if not os.path.exists(save_folder):
#     os.mkdir(save_folder)
# uv_h = uv_w = 256
# image_h = image_w = 256
# uv_coords = process_uv(uv_coords, uv_h, uv_w)
# uv_position_map = render_colors(uv_coords, triangles, vertices, uv_h, uv_w, c=3)
# io.imsave('{}/uv_padding_position_map00.png'.format(save_folder), (uv_position_map)/max(image_h, image_w))
