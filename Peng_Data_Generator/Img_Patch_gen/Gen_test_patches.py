# import numpy as np
# import cv2
# from scipy   import misc
# import scipy
import cv2
import multiprocessing as mp
import time
import os
import sys
import numpy as np
from skimage import io
import Peng_Data_Generator.AllFunctions.BP4DDataFullPathReader as BP4DFileNameLoader
from Peng_Data_Generator.AllFunctions.UVGeneratorNew import UvGenerator
from Peng_Data_Generator.AllFunctions.CommonFunctions import process_uv, render_colors,render_mask_colors
from Peng_Data_Generator.AllFunctions.BP4DDataFullPathReader import allSubjects_BP4D,allTasks_BP4D,DATA_AU_OCC,DATA_AU_INT,DRMLOccAUs_BP
from Peng_Data_Generator.Img_Patch_gen.pickPoints import pick_points,is_in_cricle

import math

start=time.time()
#Data directories
full_uv_save_folder = '/mnt/Core/BP4D_Peng_Nov_29/BP4D_Clean_UV_test_patch'

uv_hole_save_folder = '/mnt/Core/BP4D_Peng_Nov_29/BP4D_hole_UV_test_patch'
# ----Mask image save path
mask_save_folder = '/mnt/Core/BP4D_Peng_Nov_29/BP4D_mask_test_patch'

patch_save_folder='/mnt/Core/BP4D_Peng_Nov_29/BP4D_patch_test_patch'

objfile_path_lists, allImageFilenames, allLabels, \
allSubjects, allTasks, \
tbndfile_path_lists = BP4DFileNameLoader.load_ground(dataDir="/media/Data1/BP4D/Sequences",
                                   groundDir="/media/Data1/BP4D/AUCoding",
                                   groundType=DATA_AU_OCC,
                                   groundRequested=DRMLOccAUs_BP,
                                   lostListFilename="/media/Data1/BP4D/badFiles_BP4D.txt",
                                   subjectsRequested=(

    'F023',
    'M018'
                                   ),
    #                                 subjectsRequested=('F009','F010'),
                                   tasksRequested= allTasks_BP4D,
                                   # tasksRequested= ('T5','T8'),
                                   facialFeaturePointPath="/media/Data1/BP4D/new3DFeatures",
                                   normalizeLabels=False,
                                   DEBUG_PRINT=True)

single_list_3d=[]
single_list_tbnd=[]

def parse_list_3d(a):
    if isinstance(a, list):
        for item in a:
            parse_list_3d(item)
    else:
        single_list_3d.append(a)

def parse_list_tbnd(a):
    if isinstance(a, list):
        for item in a:
            parse_list_tbnd(item)
    else:
        single_list_tbnd.append(a)

parse_list_3d(objfile_path_lists)
parse_list_tbnd(tbndfile_path_lists)

imageCnt=len(single_list_3d)
imageCnt0=len(single_list_tbnd)

# ----Image generate once
def myFunc(obj_path, tbnd_path, full_uv_save_folder,uv_hole_save_folder,mask_save_folder,patch_save_folder):
    uv_map = UvGenerator().gen_uv_space(objFile=obj_path, tbndFile=tbnd_path, maxHoleCount=3, maxHoleSize=20,
                                        holeshapstype=0)


    # ----load mesh data, & UV space coords----
    holy_uv_array, holy_tris, holycloud, mask_uv_array, mask_tris, mask_color, full_uv_array, full_tris, coords = uv_map

    # ---width,height---
    # UV enlarging
    uv_h = uv_w = 64

    # ---render color for full uv without holes
    # starttime = time.time()
    full_uv_array = process_uv(full_uv_array, uv_h, uv_w)
    full_uv_position_map = render_colors(full_uv_array, full_tris, coords, uv_h, uv_w, c=3)


    mask_uv_array = process_uv(mask_uv_array, uv_h, uv_w)
    uv_mask_map = render_mask_colors(mask_uv_array, mask_tris, mask_color, uv_h, uv_w, c=3)

    holy_uv_array = process_uv(holy_uv_array, uv_h, uv_w)
    uv_position_map = render_colors(holy_uv_array, holy_tris, holycloud, uv_h, uv_w, c=3)



    path, filename = os.path.split(obj_path)
    list = path.split("/")

    uv_p = list[-2:]
    uv_p_subject = uv_p[0]
    uv_p_task = uv_p[1]
    uv_path = '/' + uv_p_subject + '/' + uv_p_task+'/'
    number = filename[:-4]
    full_uv_path=full_uv_save_folder + uv_path
    hole_uv_path=uv_hole_save_folder + uv_path
    mask_path=mask_save_folder + uv_path
    patch_path=patch_save_folder+uv_path


    return full_uv_position_map,\
           uv_mask_map,\
           uv_position_map,\
           full_uv_path,\
           hole_uv_path,\
           mask_path, \
           patch_path,\
           number

    # imageRescale_high_contrast = 150 #90
    # maskimageRescale=255
    # DEBUG
    # print("Type of array:", type(full_uv_position_map))
    # print("More type data:", full_uv_position_map.shape)
    # print("Type of element:", full_uv_position_map.dtype)
    # print("Max value:", full_uv_position_map.max())
    # print("Min value:", full_uv_position_map.min())
    # END DEBUG

    #save
    #save full uv image without holes

    #save uv


    # # save full UV images
    # minVal = full_uv_position_map.min()
    # maxVal = full_uv_position_map.max()
    # if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
    #     print("WARNING: Data does not fit to range:", minVal, maxVal)
    #
    # full_uv_position_map = np.minimum(full_uv_position_map, imageRescale_high_contrast)
    # full_uv_position_map = np.maximum(full_uv_position_map, -imageRescale_high_contrast)
    #
    # if os.path.exists(full_uv_save_folder + uv_path):
    #     pass
    # else:
    #     os.makedirs(full_uv_save_folder + uv_path, mode=0o777)
    # image_save_path = full_uv_save_folder + uv_path
    # io.imsave('{}.png'.format(image_save_path + number), (full_uv_position_map)/imageRescale_high_contrast)

    #save hole uv
    # minVal = uv_position_map.min()
    # maxVal = uv_position_map.max()
    # if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
    #     print("WARNING: Data does not fit to range:", minVal, maxVal)
    #
    # uv_position_map = np.minimum(uv_position_map, imageRescale_high_contrast)
    # uv_position_map = np.maximum(uv_position_map, -imageRescale_high_contrast)
    #
    # if os.path.exists(uv_hole_save_folder + uv_path):
    #     pass
    # else:
    #     os.makedirs(uv_hole_save_folder + uv_path, mode=0o777)
    # image_save_path = uv_hole_save_folder + uv_path
    # io.imsave('{}.png'.format(image_save_path + number), (uv_position_map)/imageRescale_high_contrast)
    #
    #
    #
    # #save UV masks
    # if os.path.exists(mask_save_folder + uv_path):
    #     pass
    # else:
    #     os.makedirs(mask_save_folder + uv_path, mode=0o777)
    # image_save_path = mask_save_folder + uv_path
    # io.imsave('{}.png'.format(image_save_path + number), (uv_mask_map)/maskimageRescale)
    #


############################test#################################
    # minVal = full_uv_position_map.min()
    # maxVal = full_uv_position_map.max()
    # if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
    #     print("WARNING: Data does not fit to range:", minVal, maxVal)
    #
    # full_uv_position_map = np.minimum(full_uv_position_map, imageRescale_high_contrast)
    # full_uv_position_map = np.maximum(full_uv_position_map, -imageRescale_high_contrast)


    # save full uv image without holes
    # full_uv_save_folder_high_contrast= '/mnt/Core/BP4D_Peng_Nov_6/BP4D_Clean_UV_high_contrast'
    # if os.path.exists(full_uv_save_folder_high_contrast + uv_path):
    #     pass
    # else:
    #     os.makedirs(full_uv_save_folder_high_contrast + uv_path, mode=0o777)
    # image_save_path = full_uv_save_folder_high_contrast + uv_path
    # io.imsave('{}.png'.format(image_save_path + number), (full_uv_position_map)/imageRescale_high_contrast)
################################test##################################











# ----MultiProcessing generating
allThreadResults = []
allOutputs = []
pool = mp.Pool(mp.cpu_count())
print("CPU count:", mp.cpu_count())

def multicore():
    for i in range(imageCnt):
        allThreadResults.append(None)
        allOutputs.append(None)

    for index in range(imageCnt):
        allThreadResults[index] = pool.apply_async(myFunc, (single_list_3d[index],
                                                            single_list_tbnd[index],
                                                            full_uv_save_folder,
                                                            uv_hole_save_folder,
                                                            mask_save_folder,
                                                            patch_save_folder))

    # Close the pool for the day...
    pool.close()
    print("Pool closed.")

    # For each thread...
    imageRescale_high_contrast = 150  # 90
    maskimageRescale = 255


    ##########define a black img#############
    blackImg = np.zeros((64, 64, 3))
    ##########define a black img#############
    #######################
    for i in range(imageCnt):
        # Get thread value
        allOutputs[i] = allThreadResults[i].get()
        (full_uv_position_map,
        uv_mask_map,
        uv_position_map,
        full_uv_path,
        hole_uv_path,
        mask_path,
        patch_path,
        number)=allOutputs[i]


        # # save full UV images
        minVal = full_uv_position_map.min()
        maxVal = full_uv_position_map.max()
        if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
            print("WARNING: Data does not fit to range:", minVal, maxVal,full_uv_path+number)

        full_uv_position_map = np.minimum(full_uv_position_map, imageRescale_high_contrast)
        full_uv_position_map = np.maximum(full_uv_position_map, -imageRescale_high_contrast)
        if os.path.exists(full_uv_path):
            pass
        else:
            os.makedirs(full_uv_path, mode=0o777)
        io.imsave('{}.png'.format(full_uv_path + number), (full_uv_position_map) / imageRescale_high_contrast)
 ##################################################################################################

        # a = np.random.randint(low=0, high=44)
        # b = np.random.randint(low=0, high=44)
        # c = np.random.randint(low=a + 15, high=a + 20)
        # d = np.random.randint(low=b + 15, high=b + 20)    dd

        a,b,c,d=is_in_cricle()
        for i in range(64):
            for j in range(64):
            # i = np.random.randint(0, 64)
            # j = np.random.randint(0, 64)
                if i >= a and i <= c and j >= b and j <= d:
                    blackImg[i, j, :] = [255, 255, 255]
            # save UV patch
        if os.path.exists(patch_path):
            pass
        else:
            os.makedirs(patch_path, mode=0o777)
        io.imsave('{}.png'.format(patch_path + number), (blackImg) / maskimageRescale)
        blackImg = np.zeros((64, 64, 3))


        # def erase_img(args, img):
        #     # save the input mask
        #     mask = cv2.imread('TestMask/2440.png')
        #     mask_save_path = 'CvMaskShow.png'
        #     cv2.imwrite(mask_save_path, mask)
        #
        #     print("mask_shape:{}".format(mask.shape))
        #     test_img = cv2.resize(img, (args.input_height, args.input_width)) / 127.5 - 1
        #     test_mask = cv2.resize(mask, (args.input_height, args.input_width)) / 255.0
        #
        #     # fill mask region to 1
        #     test_img = (test_img * (1 - test_mask)) + test_mask
        #
        #     # cv2.destroyAllWindows()
        #     return np.tile(test_img[np.newaxis, ...], [args.batch_size, 1, 1, 1]), np.tile(test_mask[np.newaxis, ...],
        #                                                                                    [args.batch_size, 1, 1, 1])
        #
        # img = cv2.imread('{}.png'.format(full_uv_path + number))
        # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        # test_img, mask = erase_img(args, img)
        # test_img = test_img.astype(np.float32)
        # test = cv2.resize((test_img[0] + 1), (int(64), int(64)))
        # test = cv2.cvtColor(test, cv2.COLOR_BGR2RGB)
        # test_save_path = 'test_image.png'
        # cv2.imwrite(test_save_path, test * (127.5 - 1))
##################################################################################################

        # save hole uv
        # minVal = uv_position_map.min()
        # maxVal = uv_position_map.max()
        # if abs(minVal) > imageRescale_high_contrast or abs(maxVal) > imageRescale_high_contrast:
        #     print("WARNING: Data does not fit to range:", minVal, maxVal)

        uv_position_map = np.minimum(uv_position_map, imageRescale_high_contrast)
        uv_position_map = np.maximum(uv_position_map, -imageRescale_high_contrast)

###############################
        # print(uv_position_map.shape)
        #
        #
        # a = np.random.randint(low=0, high=64)
        # b = np.random.randint(low=0, high=64)
        # c = np.random.randint(low=a+15, high=a+20)
        # d = np.random.randint(low=b+15, high=b+20)
        #
        # i = np.random.randint(0, 64)
        # j = np.random.randint(0, 64)
        # if j > a and i < c and j > b and i < d:
        #     blackImg[i,j,:]=[130,130,130]
        #
        # full_uv_position_map=full_uv_position_map+blackImg
        # if os.path.exists(full_uv_path):
        #     pass
        # else:
        #     os.makedirs(full_uv_path, mode=0o777)
        # io.imsave('{}.png'.format(full_uv_path + number), (full_uv_position_map) / imageRescale_high_contrast)
##################################

        if os.path.exists(hole_uv_path):
            pass
        else:
            os.makedirs(hole_uv_path, mode=0o777)
        io.imsave('{}.png'.format(hole_uv_path + number), (uv_position_map) / imageRescale_high_contrast)


        # save UV masks
        if os.path.exists(mask_path):
            pass
        else:
            os.makedirs(mask_path, mode=0o777)
        io.imsave('{}.png'.format(mask_path + number), (uv_mask_map) / maskimageRescale)
    pool.join()
    print("All threads joined.")
multicore()
end=time.time()
print("cost per object",(end-start))