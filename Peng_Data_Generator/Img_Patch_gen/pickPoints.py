import numpy as np
import math
def pick_points():
    a = np.random.randint(low=0, high=44)
    b = np.random.randint(low=0, high=44)
    c = np.random.randint(low=a + 15, high=a + 20)
    d = np.random.randint(low=b + 15, high=b + 20)
    return a,b,c,d


def is_in_cricle():

    a, b, c, d=pick_points()
    if (math.pow((c - 32), 2) + math.pow((b - 32), 2) <= math.pow(32, 2)) and (math.pow((c - 32), 2) + math.pow((d - 32), 2) <= math.pow(32, 2)) and (math.pow((a - 32), 2) + math.pow((b - 32), 2) <= math.pow(32, 2)) and (math.pow((a - 32), 2) + math.pow((d - 32), 2) <= math.pow(32, 2)):

        return a,b,c,d
    else:
        return is_in_cricle()