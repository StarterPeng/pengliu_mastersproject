import os
from os import path
from skimage import io
from PIL import Image
import numpy as np


input_dir="/mnt/Core/BP4D_Peng_Nov_9/BP4D_mask"
image_save_path="/mnt/Core/BP4D_Peng_Nov_9/BP4D_Bicolor_mask"

object_list=os.listdir(input_dir)
print("object_list",object_list)
for obj in object_list:
    obj_path=path.join(input_dir,obj)
    task_list = os.listdir(obj_path)
    print("task_list", task_list)
    for task in task_list:
        task_path=path.join(obj_path,task)
        # print("task_path", task_path)
        file_list=os.listdir(task_path)
        print("file_list", file_list)
        for file in file_list:
            file_path=path.join(task_path,file)
            # print("file_path",file_path)
            img = Image.open(file_path, mode="r")
            width = img.size[0]
            height = img.size[1]

            for x in range(width):
                for y in range(height):
                    r, g, b = img.getpixel((x, y))
                    if (r > 0):
                        img.putpixel((x, y), (1, 1, 1))
                    else:
                        img.putpixel((x, y), (0, 0, 0))

            img = np.array(img)
            if not os.path.exists(image_save_path + "/"+obj+"/"+task+"/"):
                os.makedirs(image_save_path + "/"+obj+"/"+task+"/", mode=0o777)
            io.imsave('{}'.format(image_save_path + "/"+obj+"/"+task+"/"+file), img)





