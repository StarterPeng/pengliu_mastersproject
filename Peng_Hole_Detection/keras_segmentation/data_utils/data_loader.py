"""
Title: Image-segmentation-keras
Author: Divam Gupta
Date: 2019
Code version: 0.3.0
Availability: https://github.com/divamgupta/image-segmentation-keras

"""

import glob
import itertools
import os
import random
import six
import numpy as np
import cv2

try:
    from tqdm import tqdm
except ImportError:
    print("tqdm not found, disabling progress bars")
    def tqdm(iter):
        return iter


from ..models.config import IMAGE_ORDERING
from .augmentation import augment_seg

DATA_LOADER_SEED = 0

# random.seed(DATA_LOADER_SEED)
# class_colors = [(random.randint(0, 255), random.randint(
#     0, 255), random.randint(0, 255)) for _ in range(5000)]
random.seed(0)
class_colors = [(random.randint(0, 255),
                 random.randint(0, 255),
                 random.randint(0, 255)) for _ in range(5000)]

class DataLoaderError(Exception):
    pass
def get_pairs_from_paths(images_path, segs_path):
	Subjects_UV = [
		'F001',
		# 'F002',
		# 'F003',
		# 'F004',
		# 'F005',
		# 'F006',
		# 'F007',
		# 'F008',
		# 'F009',
		# 'F010',
		# 'F011',
		# 'F012',
		# 'F013',
		# 'F014',
		# 'M001',
		# 'M002',
		# 'M003',
		# 'M004',
		# 'M005',
		# 'M006',
		# 'M007',
		# 'M008',
		# 'M009',
		# 'M010',
		# 'M011',
		# 'M012',
		# 'M013',
		# 'M014',

	]
	Tasks_UV = [
		"T1",
		# "T2",
		# "T3",
		# "T4",
		# "T5",
		# "T6",
		# "T7",
		# "T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	text_save('UVlist.txt', UVfilesList)
	text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret

# def get_pairs_from_paths(images_path, segs_path, ignore_non_matching=False):
#     """ Find all the images from the images_path directory and
#         the segmentation images from the segs_path directory
#         while checking integrity of data """
#
#     ACCEPTABLE_IMAGE_FORMATS = [".jpg", ".jpeg", ".png"]
#     ACCEPTABLE_SEGMENTATION_FORMATS = [".png"]
#
#     image_files = []
#     segmentation_files = {}
#
#     for dir_entry in os.listdir(images_path):
#         if os.path.isfile(os.path.join(images_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_IMAGE_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             image_files.append((file_name, file_extension, os.path.join(images_path, dir_entry)))
#
#     for dir_entry in os.listdir(segs_path):
#         if os.path.isfile(os.path.join(segs_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_SEGMENTATION_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             if file_name in segmentation_files:
#                 raise DataLoaderError("Segmentation file with filename {0} already exists and is ambiguous to resolve with path {1}. Please remove or rename the latter.".format(file_name, os.path.join(segs_path, dir_entry)))
#             segmentation_files[file_name] = (file_extension, os.path.join(segs_path, dir_entry))
#
#     return_value = []
#     # Match the images and segmentations
#     for image_file, _, image_full_path in image_files:
#         if image_file in segmentation_files:
#             return_value.append((image_full_path, segmentation_files[image_file][1]))
#         elif ignore_non_matching:
#             continue
#         else:
#             # Error out
#             raise DataLoaderError("No corresponding segmentation found for image {0}.".format(image_full_path))
#
#     return return_value
def get_pairs_from_paths_val_set(images_path, segs_path):
	Subjects_UV = [
		'F015',
		# 'F016',
		# 'F017',
		# 'F018',
		# 'F019',
		# 'F020',
		# 'F021',
		# 'F022',
		# 'M015',
		# 'M016',
		# 'M017',


	]
	Tasks_UV = [
		"T1",
		# "T2",
		# "T3",
		# "T4",
		# "T5",
		# "T6",
		# "T7",
		# "T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	text_save('UVlist.txt', UVfilesList)
	text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret

# def get_pairs_from_paths(images_path, segs_path, ignore_non_matching=False):
#     """ Find all the images from the images_path directory and
#         the segmentation images from the segs_path directory
#         while checking integrity of data """
#
#     ACCEPTABLE_IMAGE_FORMATS = [".jpg", ".jpeg", ".png"]
#     ACCEPTABLE_SEGMENTATION_FORMATS = [".png"]
#
#     image_files = []
#     segmentation_files = {}
#
#     for dir_entry in os.listdir(images_path):
#         if os.path.isfile(os.path.join(images_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_IMAGE_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             image_files.append((file_name, file_extension, os.path.join(images_path, dir_entry)))
#
#     for dir_entry in os.listdir(segs_path):
#         if os.path.isfile(os.path.join(segs_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_SEGMENTATION_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             if file_name in segmentation_files:
#                 raise DataLoaderError("Segmentation file with filename {0} already exists and is ambiguous to resolve with path {1}. Please remove or rename the latter.".format(file_name, os.path.join(segs_path, dir_entry)))
#             segmentation_files[file_name] = (file_extension, os.path.join(segs_path, dir_entry))
#
#     return_value = []
#     # Match the images and segmentations
#     for image_file, _, image_full_path in image_files:
#         if image_file in segmentation_files:
#             return_value.append((image_full_path, segmentation_files[image_file][1]))
#         elif ignore_non_matching:
#             continue
#         else:
#             # Error out
#             raise DataLoaderError("No corresponding segmentation found for image {0}.".format(image_full_path))
#
#     return return_value



def get_image_array(image_input, width, height, imgNorm="sub_mean",
                  ordering='channels_first'):
    """ Load image array from input """

    if type(image_input) is np.ndarray:
        # It is already an array, use it as it is
        img = image_input
        # ######################################
        # print("image color:{}".format(img.shape))
        # #########################################
        #
        # w,h,channel=img.shape
        # txt_file_name = 'uvHoleImagePixels.txt'
        # txt = open(txt_file_name, 'w')
        # for i in range(w):
        #     for j in range(h):
        #         txt.write(str(img[j, i]))
        #         txt.write('\n')
        # txt.close()
        #########################################
        ######################################
    elif  isinstance(image_input, six.string_types)  :
        if not os.path.isfile(image_input):
            raise DataLoaderError("get_image_array: path {0} doesn't exist".format(image_input))
        img = cv2.imread(image_input, 1)
        ######################################
        print("image color:{}".format(img))
        ######################################
    else:
        raise DataLoaderError("get_image_array: Can't process input type {0}".format(str(type(image_input))))

####################################
    if imgNorm == None:
        img=img
####################################

    if imgNorm == "sub_and_divide":
        img = np.float32(cv2.resize(img, (width, height))) / 127.5 - 1
    elif imgNorm == "sub_mean":
        img = cv2.resize(img, (width, height))
        img = img.astype(np.float32)
        img[:, :, 0] -= 103.939
        img[:, :, 1] -= 116.779
        img[:, :, 2] -= 123.68
        img = img[:, :, ::-1]
    elif imgNorm == "divide":
        img = cv2.resize(img, (width, height))
        img = img.astype(np.float32)
        img = img/255.0

    if ordering == 'channels_first':
        img = np.rollaxis(img, 2, 0)
    return img
def get_segmentation_array(image_input, nClasses, width, height, no_reshape=False):
    """ Load segmentation array from input """

    seg_labels = np.zeros((height, width, nClasses))

    if type(image_input) is np.ndarray:
        # It is already an array, use it as it is
        img = image_input
    elif isinstance(image_input, six.string_types) :
        if not os.path.isfile(image_input):
            raise DataLoaderError("get_segmentation_array: path {0} doesn't exist".format(image_input))
        img = cv2.imread(image_input, 1)
    else:
        raise DataLoaderError("get_segmentation_array: Can't process input type {0}".format(str(type(image_input))))

    img = cv2.resize(img, (width, height), interpolation=cv2.INTER_NEAREST)
    img = img[:, :, 0]

    for c in range(nClasses):
        seg_labels[:, :, c] = (img == c).astype(int)

    if not no_reshape:
        seg_labels = np.reshape(seg_labels, (width*height, nClasses))

    return seg_labels





def verify_segmentation_dataset(images_path, segs_path, n_classes, show_all_errors=False):
    try:
        img_seg_pairs = get_pairs_from_paths(images_path, segs_path)
        if not len(img_seg_pairs):
            print("Couldn't load any data from images_path: {0} and segmentations path: {1}".format(images_path, segs_path))
            return False

        return_value = True
        for im_fn, seg_fn in tqdm(img_seg_pairs):
            img = cv2.imread(im_fn)
            seg = cv2.imread(seg_fn)
            # Check dimensions match
            if not img.shape == seg.shape:
                return_value = False
                print("The size of image {0} and its segmentation {1} doesn't match (possibly the files are corrupt).".format(im_fn, seg_fn))
                if not show_all_errors:
                    break
            else:
                max_pixel_value = np.max(seg[:, :, 0])
                if max_pixel_value >= n_classes:
                    return_value = False
                    print("The pixel values of the segmentation image {0} violating range [0, {1}]. Found maximum pixel value {2}".format(seg_fn, str(n_classes - 1), max_pixel_value))
                    if not show_all_errors:
                        break
        if return_value:
            print("Dataset verified! ")
        else:
            print("Dataset not verified!")
        return return_value
    except DataLoaderError as e:
        print("Found error during data loading\n{0}".format(str(e)))
        return False

def verify_segmentation_dataset_val_set(images_path, segs_path, n_classes, show_all_errors=False):
    try:
        img_seg_pairs = get_pairs_from_paths_val_set(images_path, segs_path)
        if not len(img_seg_pairs):
            print("Couldn't load any data from images_path: {0} and segmentations path: {1}".format(images_path, segs_path))
            return False

        return_value = True
        for im_fn, seg_fn in tqdm(img_seg_pairs):
            img = cv2.imread(im_fn)
            seg = cv2.imread(seg_fn)
            # Check dimensions match
            if not img.shape == seg.shape:
                return_value = False
                print("The size of image {0} and its segmentation {1} doesn't match (possibly the files are corrupt).".format(im_fn, seg_fn))
                if not show_all_errors:
                    break
            else:
                max_pixel_value = np.max(seg[:, :, 0])
                if max_pixel_value >= n_classes:
                    return_value = False
                    print("The pixel values of the segmentation image {0} violating range [0, {1}]. Found maximum pixel value {2}".format(seg_fn, str(n_classes - 1), max_pixel_value))
                    if not show_all_errors:
                        break
        if return_value:
            print("Dataset verified! ")
        else:
            print("Dataset not verified!")
        return return_value
    except DataLoaderError as e:
        print("Found error during data loading\n{0}".format(str(e)))
        return False



def image_segmentation_generator(images_path, segs_path, batch_size,
                                 n_classes, input_height, input_width,
                                 output_height, output_width,
                                 do_augment=False):

    img_seg_pairs = get_pairs_from_paths(images_path, segs_path)
    random.shuffle(img_seg_pairs)
    zipped = itertools.cycle(img_seg_pairs)

    while True:
        X = []
        Y = []
        for _ in range(batch_size):
            im, seg = next(zipped)

            im = cv2.imread(im, 1)
            seg = cv2.imread(seg, 1)

            if do_augment:
                im, seg[:, :, 0] = augment_seg(im, seg[:, :, 0])

            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield np.array(X), np.array(Y)
def image_segmentation_generator_val_set(images_path, segs_path, batch_size,
                                 n_classes, input_height, input_width,
                                 output_height, output_width,
                                 do_augment=False):

    img_seg_pairs = get_pairs_from_paths_val_set(images_path, segs_path)
    random.shuffle(img_seg_pairs)
    zipped = itertools.cycle(img_seg_pairs)

    while True:
        X = []
        Y = []
        for _ in range(batch_size):
            im, seg = next(zipped)

            im = cv2.imread(im, 1)
            seg = cv2.imread(seg, 1)

            if do_augment:
                im, seg[:, :, 0] = augment_seg(im, seg[:, :, 0])

            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield np.array(X), np.array(Y)

####################################################
def get_pairs_from_paths_test_set(images_path, segs_path):
	Subjects_UV = [
		'F023',
		'M018',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	text_save('UVlist.txt', UVfilesList)
	text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret

###################################################
def image_segmentation_generator_test_set(images_path, segs_path, batch_size,
                                 n_classes, input_height, input_width,
                                 output_height, output_width,
                                 do_augment=False):

    img_seg_pairs = get_pairs_from_paths_val_set(images_path, segs_path)
    random.shuffle(img_seg_pairs)
    zipped = itertools.cycle(img_seg_pairs)

    while True:
        X = []
        Y = []
        for _ in range(batch_size):
            im, seg = next(zipped)

            im = cv2.imread(im, 1)
            seg = cv2.imread(seg, 1)

            if do_augment:
                im, seg[:, :, 0] = augment_seg(im, seg[:, :, 0])

            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield [np.array(X), np.array(Y)]
################################################################



def get_imgs_from_paths_test_set(images_path):
	Subjects_UV = [
		'F023',
		'M018',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList
	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))


	ret = []
	for i in range(len(UVfilesList)):
		ret.append(UVfilesList[i])
	return ret

def get_masks_from_paths_test_set(segs_path):
	Subjects_UV = [
		'F023',
		'M018',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]

	MaskfileList = []

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList
		print("Successfully saved")
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("Masks", len(MaskfileList))

	ret = []
	for i in range(len(MaskfileList)):
		ret.append(MaskfileList[i])
	return ret

def image_segmentation_generator_test_set_img(images_path, batch_size,
                                 input_height, input_width,
                                 ):

    imgs = get_imgs_from_paths_test_set(images_path)
    zipped = itertools.cycle(imgs)

    while True:
        X = []
        for _ in range(batch_size):
            im = next(zipped)

            im = cv2.imread(im, 1)
            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))


        yield np.array(X)
def image_segmentation_generator_test_set_mask(segs_path, batch_size,
                                 n_classes,
                                 output_height, output_width,
                                 do_augment=False):

    segs = get_masks_from_paths_test_set(segs_path)
    zipped = itertools.cycle(segs)

    while True:
        Y = []
        for _ in range(batch_size):
            seg = next(zipped)

            seg = cv2.imread(seg, 1)

            if do_augment:
                im, seg[:, :, 0] = augment_seg(im, seg[:, :, 0])
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield np.array(Y)