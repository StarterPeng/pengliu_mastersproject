"""
Title: Image-segmentation-keras
Author: Divam Gupta
Date: 2019
Code version: 0.3.0
Availability: https://github.com/divamgupta/image-segmentation-keras

"""
from . import unet
model_from_name = {}
model_from_name["my_unet"] = unet.my_unet

