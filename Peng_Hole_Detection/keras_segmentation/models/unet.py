from keras.models import *
from keras.layers import *
from .config import IMAGE_ORDERING
from .model_utils import get_segmentation_model_my_unet



if IMAGE_ORDERING == 'channels_first':
    MERGE_AXIS = 1
elif IMAGE_ORDERING == 'channels_last':
    MERGE_AXIS = -1


smooth=1
def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


def my_unet(n_classes,input_height=64, input_width=64):
    if IMAGE_ORDERING == 'channels_first':
        img_input = Input(shape=(3, input_height, input_width))
    elif IMAGE_ORDERING == 'channels_last':
        img_input = Input(shape=(input_height, input_width, 3))

    c_layer1= Conv2D(32, (3, 3),
                     activation='relu',
                     padding='same')(img_input)
    c_layer1 = Conv2D(32, (3, 3), activation='relu', padding='same')(c_layer1)
    p_layer1 = MaxPooling2D(pool_size=(2, 2))(c_layer1)

    c_layer2 = Conv2D(64, (3, 3), activation='relu', padding='same')(p_layer1)
    c_layer2 = Conv2D(64, (3, 3), activation='relu', padding='same')(c_layer2)
    p_layer2 = MaxPooling2D(pool_size=(2, 2))(c_layer2)

    c_layer3 = Conv2D(128, (3, 3), activation='relu', padding='same')(p_layer2)
    c_layer3 = Conv2D(128, (3, 3), activation='relu', padding='same')(c_layer3)
    p_layer3 = MaxPooling2D(pool_size=(2, 2))(c_layer3)

    c_layer4 = Conv2D(256, (3, 3), activation='relu', padding='same')(p_layer3)
    c_layer4 = Conv2D(256, (3, 3), activation='relu', padding='same')(c_layer4)
    p_layer4 = MaxPooling2D(pool_size=(2, 2))(c_layer4)

    c_layer5 = Conv2D(512, (3, 3), activation='relu', padding='same')(p_layer4)
    c_layer5 = Conv2D(512, (3, 3), activation='relu', padding='same')(c_layer5)

    up_layer1 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(c_layer5), c_layer4], axis=3)
    c_layer6 = Conv2D(256, (3, 3), activation='relu', padding='same')(up_layer1)
    c_layer6 = Conv2D(256, (3, 3), activation='relu', padding='same')(c_layer6)

    up_layer2 = concatenate([Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c_layer6), c_layer3], axis=3)
    c_layer7 = Conv2D(128, (3, 3), activation='relu', padding='same')(up_layer2)
    c_layer7 = Conv2D(128, (3, 3), activation='relu', padding='same')(c_layer7)

    up_layer3 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c_layer7), c_layer2], axis=3)
    c_layer8 = Conv2D(64, (3, 3), activation='relu', padding='same')(up_layer3)
    c_layer8 = Conv2D(64, (3, 3), activation='relu', padding='same')(c_layer8)

    up_layer4 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c_layer8), c_layer1], axis=3)
    c_layer9 = Conv2D(32, (3, 3), activation='relu', padding='same')(up_layer4)
    c_layer9 = Conv2D(32, (3, 3), activation='relu', padding='same')(c_layer9)


    model = get_segmentation_model_my_unet(img_input, c_layer9)
    model.model_name = "my_unet"
    return model




















if __name__ == '__main__':
    m = unet_mini(101)
    m = _unet(101, vanilla_encoder)
    # m = _unet( 101 , get_mobilenet_encoder ,True , 224 , 224  )
    m = _unet(101, get_vgg_encoder)
    m = _unet(101, get_resnet50_encoder)
