"""
Title: Image-segmentation-keras
Author: Divam Gupta
Date: 2019
Code version: 0.3.0
Availability: https://github.com/divamgupta/image-segmentation-keras

"""
import argparse
import json
from Peng_Hole_Detection.keras_segmentation.data_utils.data_loader import image_segmentation_generator,verify_segmentation_dataset,verify_segmentation_dataset_val_set,image_segmentation_generator_val_set,image_segmentation_generator_test_set_img,image_segmentation_generator_test_set_mask,image_segmentation_generator_test_set
import keras
import os
import glob
import six
from keras.callbacks import TensorBoard,ModelCheckpoint,EarlyStopping
from keras import backend as K


def find_latest_checkpoint(checkpoints_path, fail_safe=True):

    def get_epoch_number_from_path(path):
        return path.replace(checkpoints_path, "").strip(".")

    # Get all matching files
    all_checkpoint_files = glob.glob(checkpoints_path + ".*")
    # Filter out entries where the epoc_number part is pure number
    all_checkpoint_files = list(filter(lambda f: get_epoch_number_from_path(f).isdigit(), all_checkpoint_files))
    if not len(all_checkpoint_files):
        # The glob list is empty, don't have a checkpoints_path
        if not fail_safe:
            raise ValueError("Checkpoint path {0} invalid".format(checkpoints_path))
        else:
            return None

    # Find the checkpoint file with the maximum epoch
    latest_epoch_checkpoint = max(all_checkpoint_files, key=lambda f: int(get_epoch_number_from_path(f)))
    return latest_epoch_checkpoint



smooth=1
def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


def train(model,
          train_images,
          train_annotations,
          input_height=None,
          input_width=None,
          n_classes=None,
          verify_dataset=True,
          checkpoints_path=None,
          logdir=None,
          epochs=5,
          batch_size=2,
          validate=False,
          val_images=None,
          val_annotations=None,
          val_batch_size=2,
          validation_steps=200,
          auto_resume_checkpoint=False,
          load_weights=None,
          steps_per_epoch=512,
          optimizer_name='adadelta',

          test=False,
          test_images=None,
          test_annotations=None,
          test_batch_size=50,
          test_step=200
          ):

    from .models.all_models import model_from_name
    # check if user gives model name instead of the model object
    if isinstance(model, six.string_types):
        # create the model from the name
        assert (n_classes is not None), "Please provide the n_classes"
        if (input_height is not None) and (input_width is not None):
            model = model_from_name[model](
                n_classes, input_height=input_height, input_width=input_width)
        else:
            model = model_from_name[model](n_classes)

    n_classes = model.n_classes
    input_height = model.input_height
    input_width = model.input_width
    output_height = model.output_height
    output_width = model.output_width

    if validate:
        assert val_images is not None
        assert val_annotations is not None

    if optimizer_name is not None:
        if optimizer_name=='sgd':
            model.compile(loss='binary_crossentropy',
                          optimizer=optimizer_name,
                          metrics=['accuracy'])
        if optimizer_name=='Adam':
            model.compile(loss='binary_crossentropy',
                          optimizer=keras.optimizers.Adam(lr=0.001),
                          metrics=['accuracy'])
    else:
        model.compile(loss='categorical_crossentropy',
                      optimizer=optimizer_name,
                      metrics=['accuracy'])


    if checkpoints_path is not None:
        with open(checkpoints_path+"_config.json", "w") as f:
            json.dump({
                "model_class": model.model_name,
                "n_classes": n_classes,
                "input_height": input_height,
                "input_width": input_width,
                "output_height": output_height,
                "output_width": output_width
            }, f)

    if load_weights is not None and len(load_weights) > 0:
        print("Loading weights from ", load_weights)
        model.load_weights(load_weights)

    if auto_resume_checkpoint and (checkpoints_path is not None):
        latest_checkpoint = find_latest_checkpoint(checkpoints_path)
        if latest_checkpoint is not None:
            print("Loading the weights from latest checkpoint ",
                  latest_checkpoint)
            model.load_weights(latest_checkpoint)

    if verify_dataset:
        print("Verifying training dataset")
        verified = verify_segmentation_dataset(train_images, train_annotations, n_classes)
        print("Training set verified")
        # assert verified
        if validate:
            print("Verifying validation dataset")
            verified = verify_segmentation_dataset_val_set(val_images, val_annotations, n_classes)
            print("Training set verified")
            # assert verified


    train_gen = image_segmentation_generator(
        train_images, train_annotations,  batch_size,  n_classes,
        input_height, input_width, output_height, output_width)

    if validate:
        val_gen = image_segmentation_generator_val_set(
            val_images, val_annotations,  val_batch_size,
            n_classes, input_height, input_width, output_height, output_width)

    if test:
        test_pairs_gen = image_segmentation_generator_test_set(
            test_images, test_annotations, test_batch_size,
            n_classes, input_height, input_width, output_height, output_width)
    if not validate:
        model.fit_generator(train_gen,
                            steps_per_epoch,
                            epochs=epochs,
                            callbacks=[TensorBoard(log_dir='logs/unetNov15')],
                            workers=1)
        if checkpoints_path is not None:
            model.save_weights(checkpoints_path + "." + str(epochs))
            print("saved ", checkpoints_path + ".model." + str(epochs))
        print("Finished Epoch", epochs)
    else:


        early_stop = EarlyStopping(monitor='val_loss',
                                   min_delta=0.00003,
                                   patience=1,
                                   mode='min',
                                   )
        model.fit_generator(train_gen,
                            steps_per_epoch,
                            validation_data=val_gen,
                            validation_steps=validation_steps,
                            epochs=epochs,
                            callbacks = [TensorBoard(log_dir=logdir,
                                                     histogram_freq=0,
                                                     write_graph=True,
                                                     write_images=1
                                                     ),
                                         early_stop,
                                           ],
                            workers = 1)
        #####################################tensorboard###########################

    if test:
        testloss, testacc = model.evaluate(x=test_pairs_gen,
                                    # batch_size=test_batch_size,
                                    steps=test_step,
                                    verbose=1)
        print('Test loss:', testloss)
        print('Test accuracy:', testacc)
        ###########################################################################

        #########save best model
        if checkpoints_path is not None:
            model.save_weights(checkpoints_path + "." + str(epochs))
            print("saved ", checkpoints_path + ".model." + str(epochs))
        print("Finished Epoch", epochs)
