"""
Title: Image-segmentation-keras
Author: Divam Gupta
Date: 2019
Code version: 0.3.0
Availability: https://github.com/divamgupta/image-segmentation-keras

"""
import json
from Peng_Hole_Detection.keras_segmentation.data_utils.data_loader import image_segmentation_generator,verify_segmentation_dataset,verify_segmentation_dataset_val_set,image_segmentation_generator_val_set,image_segmentation_generator_test_set_img,image_segmentation_generator_test_set_mask,image_segmentation_generator_test_set
import keras
import os
import glob
import six
from Peng_Hole_Detection.keras_segmentation.models.unet import my_unet



def find_latest_checkpoint(checkpoints_path, fail_safe=True):

    def get_epoch_number_from_path(path):
        return path.replace(checkpoints_path, "").strip(".")

    # Get all matching files
    all_checkpoint_files = glob.glob(checkpoints_path + ".*")
    # Filter out entries where the epoc_number part is pure number
    all_checkpoint_files = list(filter(lambda f: get_epoch_number_from_path(f).isdigit(), all_checkpoint_files))
    if not len(all_checkpoint_files):
        # The glob list is empty, don't have a checkpoints_path
        if not fail_safe:
            raise ValueError("Checkpoint path {0} invalid".format(checkpoints_path))
        else:
            return None

    # Find the checkpoint file with the maximum epoch
    latest_epoch_checkpoint = max(all_checkpoint_files, key=lambda f: int(get_epoch_number_from_path(f)))
    return latest_epoch_checkpoint


def testing(model,
          input_height=64,
          input_width=64,
          n_classes=2,
          checkpoints_path='/mnt/Core/BestModelNov19th/myunetepoch6/MyUnetNov20th',
          test=True,
          test_images="/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV",
          test_annotations="/mnt/Core/BP4D_Peng_Nov_9/BP4D_Bicolor_mask",
          test_batch_size=50,
          test_step=200
          ):

    from .models.all_models import model_from_name
    # check if user gives model name instead of the model object
    if isinstance(model, six.string_types):
        # create the model from the name
        assert (n_classes is not None), "Please provide the n_classes"
        if (input_height is not None) and (input_width is not None):
            model = model_from_name[model](
                n_classes, input_height=input_height, input_width=input_width)
        else:
            model = model_from_name[model](n_classes)

    n_classes = model.n_classes
    input_height = model.input_height
    input_width = model.input_width
    output_height = model.output_height
    output_width = model.output_width



    if checkpoints_path is not None:
        with open(checkpoints_path+"_config.json", "w") as f:
            json.dump({
                "model_class": model.model_name,
                "n_classes": n_classes,
                "input_height": input_height,
                "input_width": input_width,
                "output_height": output_height,
                "output_width": output_width
            }, f)


    latest_checkpoint = find_latest_checkpoint(checkpoints_path)
    if latest_checkpoint is not None:
        print("Loading the weights from latest checkpoint ",
                latest_checkpoint)
        model.load_weights(latest_checkpoint)

    if test:
        test_pairs_gen = image_segmentation_generator_test_set(
            test_images, test_annotations, test_batch_size,
            n_classes, input_height, input_width, output_height, output_width)

        score, acc = model.evaluate(x=test_pairs_gen,
                                    steps=test_step,
                                    verbose=1)
        print('Test score:', score)
        print('Test accuracy:', acc)

model = my_unet(n_classes=2 ,  input_height=64, input_width=64  )

model.testing(model,
          input_height=64,
          input_width=64,
          n_classes=2,
          checkpoints_path='/mnt/Core/BestModelNov19th/myunetepoch6/MyUnetNov20th',
          test=True,
          test_images="/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV",
          test_annotations="/mnt/Core/BP4D_Peng_Nov_9/BP4D_Bicolor_mask",
          test_batch_size=50,
          test_step=200)