import os
import sys
# sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
# sys.path.append('/home/liup/Code/Python/masterproject_Hole_Refinement/pengliu_mastersproject/Peng_Hole_Detection/training_unet.py')
sys.path.append('../')
from Peng_Hole_Detection.keras_segmentation.models.unet import my_unet

model = my_unet(n_classes=2 ,  input_height=64, input_width=64  )

model.train(
    train_images="/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV",
    train_annotations="/mnt/Core/BP4D_Peng_Nov_9/BP4D_Bicolor_mask",
    epochs=50,
    n_classes=2,
    batch_size=128,
    steps_per_epoch=512,
    optimizer_name='Adam',
    validate=True,
    val_images="/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV",
    val_annotations="/mnt/Core/BP4D_Peng_Nov_9/BP4D_Bicolor_mask",
    val_batch_size=128,
    validation_steps=64,
    checkpoints_path = "/mnt/Core/Unet_checkpoint/MyUnetDec4th",
    logdir='logs/myUnetDec4th',
    auto_resume_checkpoint=False,

    test=True,
    test_images="/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV",
    test_annotations="/mnt/Core/BP4D_Peng_Nov_9/BP4D_Bicolor_mask",
    test_batch_size=64,
    test_step=32

)

out_my_unet = model.predict_segmentation(
    inp="/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV/F001/T1/2440.png",
    out_fname="myunetoutNov26.png"
)
