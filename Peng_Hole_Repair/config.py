"""
Title: Globally and Locally Consistent Image Completion
Author: Tony Shin
Date: 2018
Code version: Unknown
Availability: https://github.com/shinseung428/GlobalLocalImageCompletion_TF

"""

import argparse

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1', True):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0', False):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(description='')

#Image setting
parser.add_argument('--input_width', dest='input_width', default=64, help='input image width')
parser.add_argument('--input_height', dest='input_height', default=64, help='input image height')
parser.add_argument('--local_input_width', dest='local_input_width', default=32, help='local input image width')
parser.add_argument('--local_input_height', dest='local_input_height', default=32, help='localinput image height')
parser.add_argument('--input_channel', dest='input_channel', default=3, help='input image channel')

parser.add_argument('--input_dim', dest='input_dim', default=100, help='input z size')

#Training Settings
parser.add_argument('--continue_training', dest='continue_training', default=True, type=str2bool, help='flag to continue training')


parser.add_argument('--data', dest='data', default='/mnt/Core/BP4D_Peng_Nov_9/BP4D_Clean_UV', help='image train path')

# parser.add_argument('--testing_data', dest='data', default='/mnt/Core/BP4D_Peng_Nov_9/BP4D_Clean_UV', help='image train path')
#64
parser.add_argument('--batch_size', dest='batch_size', default=128, help='batch size')
#400
parser.add_argument('--train_step', dest='train_step', default=512, help='total number of train_step')
#100
parser.add_argument('--Tc', dest='Tc', default=100, help='Tc to train Completion Network')
parser.add_argument('--Td', dest='Td', default=1, help='Td to train Discriminator Network')


parser.add_argument('--learning_rate', dest='learning_rate', default=0.0005, help='learning rate of the optimizer')
#beta 1
parser.add_argument('--momentum', dest='momentum', default=0.9, help='momentum of the optimizer')

#I set alpha to 1 to give more weights to the discriminator loss
parser.add_argument('--alpha', dest='alpha', default=1.0, help='alpha')
parser.add_argument('--margin', dest='margin', default=5, help='margin')

#Test image
# parser.add_argument('--img_path', dest='img_path', default='TestFile/007.png', help='test image path')
parser.add_argument('--img_path', dest='img_path', default='2440.png', help='test image path')

#Extra folders setting
# parser.add_argument('--checkpoints_path', dest='checkpoints_path', default='unet_checkpoints', help='saved model checkpoint path')
parser.add_argument('--checkpoints_path', dest='checkpoints_path', default='RepairModel', help='saved model checkpoint path')
parser.add_argument('--graph_path', dest='graph_path', default='graphs', help='tensorboard graph')

parser.add_argument('--images_path', dest='images_path', default='result_images', help='result images path')

parser.add_argument('--evaluation_data',
                    dest='evaluation_data',
                    default='/mnt/Core/BP4D_Peng_Nov_9/BP4D_hole_UV',
                    help='model evaluation hole image path')
parser.add_argument('--evaluation_mask_data',
                    dest='evaluation_mask_data',
                    default='/mnt/Core/BP4D_Peng_Nov_29/Patch_Test_Set',
                    help='model evaluation mask path')
parser.add_argument('--evaluation_orig_data',
                    dest='evaluation_orig_data',
                    default='/mnt/Core/BP4D_Peng_Nov_9/BP4D_Clean_UV',
                    help='model evaluation orig image path')

parser.add_argument('--subjects',
                    dest='subjects',
                    default=[
    # 'F001',
    # 'F002',
    # 'F003',
    # 'F004',
    # 'F005',
    # 'F006',
    # 'F007',
    # 'F008',
    # 'F009',
    # 'F010',
    # 'F011',
    # 'F012',
    # 'F013',
    # 'F014',
    # 'M001',
    # 'M002',
    # 'M003',
    # 'M004',
    # 'M005',
    # 'M006',
    # 'M007',
    # 'M008',
    # 'M009',
    # 'M010',
    # 'M011',
    # 'M012',
    # 'M013',
    # 'M014',
    'F023'
    # 'M018'
],
                    help='subjects')

parser.add_argument('--tasks',
                    dest='tasks',
                    default=[
    "T1",
    "T2",
    # "T3",
    # "T4",
    # "T5",
    # "T6",
    # "T7",
    # "T8"
],
                    help='tasks')


args = parser.parse_args()