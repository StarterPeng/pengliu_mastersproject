"""
Title: Globally and Locally Consistent Image Completion
Author: Tony Shin
Date: 2018
Code version: Unknown
Availability: https://github.com/shinseung428/GlobalLocalImageCompletion_TF

"""

from glob import glob
import os
import tensorflow as tf
import math

def block_patch(input, margin=5):
	shape = input.get_shape().as_list()
	#create patch in random size
	pad_size = tf.random_uniform([2], minval=15, maxval=20, dtype=tf.int32)
	patch = tf.zeros([pad_size[0], pad_size[1], shape[-1]], dtype=tf.float32)

	while True:
		h_ = tf.random_uniform([1], minval=margin, maxval=shape[0]-pad_size[0]-margin, dtype=tf.int32)[0]
		w_ = tf.random_uniform([1], minval=margin, maxval=shape[1]-pad_size[1]-margin, dtype=tf.int32)[0]
		padding = [[h_, shape[0]-h_-pad_size[0]], [w_, shape[1]-w_-pad_size[1]], [0, 0]]
		if(is_inside_circle(shape[0],h_,w_,pad_size[0],pad_size[1])):
			break

	# print("padding:{}".format(padding.eval()))
	padded = tf.pad(patch, padding, "CONSTANT", constant_values=1)
	coord = h_, w_

	res = tf.multiply(input, padded)

	return res, padded, coord, pad_size


def is_inside_circle(shape,h_,w_,pad_size_h,pad_size_w):
	vertex_count = int(0)
	radis = int(shape/2)
	center = int(shape/2)

	if (math.pow(h_.eval() - center,2) + math.pow(w_.eval()- center,2) < math.pow(radis,2)):
		vertex_count = vertex_count + 1
	if (math.pow(h_.eval() - center, 2) + math.pow(w_.eval() + pad_size_w.eval() - center, 2) < math.pow(radis,2)):
		vertex_count = vertex_count + 1
	if (math.pow(h_.eval() + pad_size_h.eval() - center, 2) + math.pow(w_.eval() - center, 2) < math.pow(radis,2)):
		vertex_count = vertex_count + 1
	if (math.pow(h_.eval() + pad_size_h.eval() - center, 2) + math.pow(w_.eval() + pad_size_w.eval() - center, 2) < math.pow(radis,2)):
		vertex_count = vertex_count + 1

	if (vertex_count < 4):
		return False

	return True


#function to get training data

def get_pairs_from_paths(images_path, segs_path):
	Subjects_UV = [
		'F001',
		'F002',
		'F003',
		'F004',
		'F005',
		'F006',
		'F007',
		'F008',
		'F009',
		'F010',
		'F011',
		'F012',
		'F013',
		'F014',
		'M001',
		'M002',
		'M003',
		'M004',
		'M005',
		'M006',
		'M007',
		'M008',
		'M009',
		'M010',
		'M011',
		'M012',
		'M013',
		'M014',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		# "T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	# text_save('UVlist.txt', UVfilesList)
	# text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret




def load_train_data(args):
	path=args.data
	Subjects_UV = [
		'F001',
		'F002',
		'F003',
		'F004',
		'F005',
		'F006',
		'F007',
		'F008',
		'F009',
		'F010',
		'F011',
		'F012',
		'F013',
		'F014',
		'M001',
		'M002',
		'M003',
		'M004',
		'M005',
		'M006',
		'M007',
		'M008',
		'M009',
		'M010',
		'M011',
		'M012',
		'M013',
		'M014',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []
	# MaskfileList = []

	def UvfilenamesReader(path, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(path, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")


	UvfilenamesReader(path, Subjects_UV, Tasks_UV)
	data_count = len(UVfilesList)
	print("data_count:{}".format(data_count))
	filename_queue = tf.train.string_input_producer(tf.train.match_filenames_once(UVfilesList))

	image_reader = tf.WholeFileReader()
	_, image_file = image_reader.read(filename_queue)
	images = tf.image.decode_png(image_file, channels=3)
	images = tf.image.resize_images(images, [args.input_height, args.input_width])
	images = tf.image.convert_image_dtype(images, dtype=tf.float32) / 127.5 - 1

	orig_images = images
	images, mask, coord, pad_size = block_patch(images, margin=args.margin)

	mask = tf.reshape(mask, [args.input_height, args.input_height, 3])


	# flip mask values
	mask = -(mask - 1)
	images += mask

	orig_imgs, perturbed_imgs, mask, coord, pad_size = tf.train.shuffle_batch(
		[orig_images, images, mask, coord, pad_size],
		batch_size=args.batch_size,
		capacity=args.batch_size * 2,
		min_after_dequeue=args.batch_size
		)

	return orig_imgs, perturbed_imgs, mask, coord, pad_size, data_count

def load_test_data(args):
	path = args.testing_data
	Subjects_UV = [
		'F023',
		'M018'

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []

	# MaskfileList = []

	def UvfilenamesReader(path, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(path, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(path, Subjects_UV, Tasks_UV)
	# paths = glob("./data/test/*.jpg")
	data_count = len(UVfilesList)

	filename_queue = tf.train.string_input_producer(tf.train.match_filenames_once(UVfilesList))

	image_reader = tf.WholeFileReader()
	_, image_file = image_reader.read(filename_queue)
	images = tf.image.decode_jpeg(image_file, channels=3)
	

	#input image range from -1 to 1
	# uncomment to center crop 
	# images = tf.image.central_crop(images, 0.5)
	images = tf.image.resize_images(images ,[args.input_height, args.input_width])
	images = tf.image.convert_image_dtype(images, dtype=tf.float32) / 127.5 - 1
	
	orig_images = images
	images, mask, coord, pad_size = block_patch(images, margin=args.margin)
	mask = tf.reshape(mask, [args.input_height, args.input_height, 3])
	# io.imsave('maskForShow.png',mask)


	#flip mask values
	mask = -(mask - 1)
	images += mask

	orig_imgs, mask, test_imgs = tf.train.batch([orig_images, mask, images],
												batch_size=args.batch_size,
												capacity=args.batch_size,
											    )


	return orig_imgs, test_imgs, mask, data_count

















# #function to save images in tile
# #comment this function block if you don't have opencv
# def img_tile(epoch, args, imgs, aspect_ratio=1.0, tile_shape=None, border=1, border_color=0):
# 	if imgs.ndim != 3 and imgs.ndim != 4:
# 		raise ValueError('imgs has wrong number of dimensions.')
# 	n_imgs = imgs.shape[0]

# 	tile_shape = None
# 	# Grid shape
# 	img_shape = np.array(imgs.shape[1:3])
# 	if tile_shape is None:
# 		img_aspect_ratio = img_shape[1] / float(img_shape[0])
# 		aspect_ratio *= img_aspect_ratio
# 		tile_height = int(np.ceil(np.sqrt(n_imgs * aspect_ratio)))
# 		tile_width = int(np.ceil(np.sqrt(n_imgs / aspect_ratio)))
# 		grid_shape = np.array((tile_height, tile_width))
# 	else:
# 		assert len(tile_shape) == 2
# 		grid_shape = np.array(tile_shape)

# 	# Tile image shape
# 	tile_img_shape = np.array(imgs.shape[1:])
# 	tile_img_shape[:2] = (img_shape[:2] + border) * grid_shape[:2] - border

# 	# Assemble tile image
# 	tile_img = np.empty(tile_img_shape)
# 	tile_img[:] = border_color
# 	for i in range(grid_shape[0]):
# 		for j in range(grid_shape[1]):
# 			img_idx = j + i*grid_shape[1]
# 			if img_idx >= n_imgs:
# 				# No more images - stop filling out the grid.
# 				break
# 			img = imgs[img_idx]
# 			img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# 			yoff = (img_shape[0] + border) * i
# 			xoff = (img_shape[1] + border) * j
# 			tile_img[yoff:yoff+img_shape[0], xoff:xoff+img_shape[1], ...] = img

# 	cv2.imwrite(args.images_path+"/img_"+str(epoch) + ".jpg", (tile_img + 1)*127.5)
