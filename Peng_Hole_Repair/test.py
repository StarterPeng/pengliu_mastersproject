"""
Title: Globally and Locally Consistent Image Completion
Author: Tony Shin
Date: 2018
Code version: Unknown
Availability: https://github.com/shinseung428/GlobalLocalImageCompletion_TF
"""

import tensorflow as tf
import numpy as np
from .config import *
from .network import *
import cv2


drawing = False # true if mouse is pressed
ix,iy = -1,-1
color = (255,255,255)
size = 5

def erase_img(args, img):

    #save the input mask
    mask = cv2.imread('TestMask/007.png')
    mask_save_path = 'CvMaskShow.png'
    cv2.imwrite(mask_save_path,mask)
    print("mask_shape:{}".format(mask.shape))
    test_img = cv2.resize(img, (args.input_height, args.input_width))/127.5 - 1
    test_mask = cv2.resize(mask, (args.input_height, args.input_width))/255.0

    #fill mask region to 1
    test_img = (test_img * (1-test_mask)) + test_mask

    # cv2.destroyAllWindows()
    return np.tile(test_img[np.newaxis,...], [args.batch_size,1,1,1]), np.tile(test_mask[np.newaxis,...], [args.batch_size,1,1,1])


def test(args, sess, model):
    #saver  
    saver = tf.train.Saver()        
    last_ckpt = tf.train.latest_checkpoint(args.checkpoints_path)
    saver.restore(sess, last_ckpt)
    ckpt_name = str(last_ckpt)
    print ("Loaded model file from " + ckpt_name)
    
    img = cv2.imread(args.img_path)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    # orig_test = cv2.resize(img, (args.input_height, args.input_width))
    orig_test = cv2.resize(img, (args.input_height, args.input_width)) / 127.5-1
    orig_test = np.tile(orig_test[np.newaxis,...],[args.batch_size,1,1,1])
    orig_test = orig_test.astype(np.float32)

    orig_w, orig_h = img.shape[0], img.shape[1]
    test_img, mask = erase_img(args, img)
    test_img = test_img.astype(np.float32)
    ##################saving mask###################

    # os.makedirs(image_save_path, mode=0o777, exist_ok=True)
    # io.imsave('{}.png'.format(image_save_path+'01'), mask)

    # print("mask_shape:{}".format(mask.shape))

    print ("Testing ...")
    res_img = sess.run(model.test_res_imgs, feed_dict={model.single_orig:orig_test,
                                                       model.single_test:test_img,
                                                       model.single_mask:mask})

    orig = cv2.resize((orig_test[0]+1), (int(orig_h), int(orig_w)))
    # orig = cv2.resize((orig_test[0]), (int(orig_h), int(orig_w)))

    # orig=int(orig)
    test = cv2.resize((test_img[0]+1), (int(orig_h), int(orig_w)))
    # test = cv2.resize((test_img[0]), (int(orig_h), int(orig_w)))


    recon = cv2.resize((res_img[0]+1), (int(orig_h), int(orig_w)))
    # recon = cv2.resize((res_img[0]), (int(orig_h), int(orig_w)))

    orig_save_path = 'TestResults/orig_image1.png'
    orig = cv2.cvtColor(orig, cv2.COLOR_BGR2RGB)
    cv2.imwrite(orig_save_path,orig*(127.5 - 1))

    test = cv2.cvtColor(test, cv2.COLOR_BGR2RGB)
    test_save_path = 'TestResults/test_image1.png'
    cv2.imwrite(test_save_path,test*(127.5 - 1))

    recon = cv2.cvtColor(recon, cv2.COLOR_BGR2RGB)
    recon_save_path = 'TestResults/recon_image1.png'
    cv2.imwrite(recon_save_path,recon*(127.5 - 1))

    # mse=tf.reduce_mean(tf.nn.l2_loss(orig - recon))
    mse = tf.nn.l2_loss(orig - recon)

    print("MSE_Single:{}".format(mse.eval()))

    # res = np.hstack([orig,test,recon])
    # res = cv2.cvtColor(res, cv2.COLOR_BGR2RGB)
    # cv2.imshow("result", res)
    # cv2.waitKey()

    print("Done.")


def main(_):
    run_config = tf.ConfigProto()
    run_config.gpu_options.allow_growth = True
    
    with tf.Session(config=run_config) as sess:
        model = network(args)

        print ('Start Testing...')
        test(args, sess, model)

main(args)
