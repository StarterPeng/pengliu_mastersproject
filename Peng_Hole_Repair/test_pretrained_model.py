"""
Title: Globally and Locally Consistent Image Completion
Author: Tony Shin
Date: 2018
Code version: Unknown
Availability: https://github.com/shinseung428/GlobalLocalImageCompletion_TF

"""

import tensorflow as tf
import numpy as np
import os
import cv2
from .config import *
from .network import *
from skimage import io
import skimage
from glob import glob
import math

Seg_mask_path = args.evaluation_mask_data
Hole_path = args.evaluation_data
orig_path = args.evaluation_orig_data
Subjects_UV=args.subjects
Tasks_UV=args.tasks

def holefilenamesReader(Hole_path, Subjects_UV, Tasks_UV):
    UVfilesList = []
    for subject in Subjects_UV:
        # Get path to image subject folder
        currentSubjectPath = os.path.join(Hole_path, subject)
        for task in Tasks_UV:
            currenttaskPath = os.path.join(currentSubjectPath, task)
            unsorted = os.listdir(currenttaskPath)
            unsorted.sort()
            for UV in unsorted:
                UVpaths = os.path.join(currenttaskPath, UV)
                UVfilesList.append(UVpaths)

    data_count = len(UVfilesList)
    print("hole_data_count:{}".format(data_count))
    return UVfilesList,data_count

def Mask_filenamesReader(Seg_mask_path, Subjects_UV, Tasks_UV):
    SegMaskList = []
    for subject in Subjects_UV:
        # Get path to image subject folder
        currentSubjectPath = os.path.join(Seg_mask_path, subject)
        for task in Tasks_UV:
            currenttaskPath = os.path.join(currentSubjectPath, task)
            unsorted = os.listdir(currenttaskPath)
            unsorted.sort()
            for UV in unsorted:
                UVpaths = os.path.join(currenttaskPath, UV)
                SegMaskList.append(UVpaths)

    data_count = len(SegMaskList)
    print("mask_data_count:{}".format(data_count))
    return SegMaskList,data_count

def orig_filenamesReader(orig_path, Subjects_UV, Tasks_UV):
    OrigUVList = []
    for subject in Subjects_UV:
        # Get path to image subject folder
        currentSubjectPath = os.path.join(orig_path, subject)
        for task in Tasks_UV:
            currenttaskPath = os.path.join(currentSubjectPath, task)
            unsorted = os.listdir(currenttaskPath)
            unsorted.sort()
            for UV in unsorted:
                UVpaths = os.path.join(currenttaskPath, UV)
                OrigUVList.append(UVpaths)

    orig_data_count = len(OrigUVList)
    print("orig_data_count:{}".format(orig_data_count))
    return OrigUVList,orig_data_count




def erase_img(args, orig_img, mask ):


    test_img = cv2.resize(orig_img, (args.input_height, args.input_width))/127.5 - 1
    test_mask = cv2.resize(mask, (args.input_height, args.input_width))/255.0

    #fill mask region to 1
    test_img = (test_img * (1-test_mask)) + test_mask

    # cv2.destroyAllWindows()
    return np.tile(test_img[np.newaxis,...], [args.batch_size,1,1,1]), np.tile(test_mask[np.newaxis,...], [args.batch_size,1,1,1])

def get_lists(orig_path,Seg_mask_path, Subjects_UV, Tasks_UV):
    mask_list, mask_cnt = Mask_filenamesReader(Seg_mask_path, Subjects_UV, Tasks_UV)
    orig_list, orig_cnt = orig_filenamesReader(orig_path, Subjects_UV, Tasks_UV)
    return mask_list, mask_cnt,orig_list, orig_cnt


def test(args, sess, model,orig_list,orig_cnt,mask_list,mask_cnt):
    # saver
    saver = tf.train.Saver()
    last_ckpt = tf.train.latest_checkpoint(args.checkpoints_path)
    saver.restore(sess, last_ckpt)
    ckpt_name = str(last_ckpt)
    print("Loaded model file from " + ckpt_name)
    ##########get img array#################
    total_mse=0
    print("orig_cnt:{}".format(orig_cnt))
    for i in range (orig_cnt):
        oirg_img = cv2.imread(orig_list[i])
        oirg_img = cv2.cvtColor(oirg_img, cv2.COLOR_RGB2BGR)
        seg_mask = cv2.imread(mask_list[i])
        seg_mask = cv2.cvtColor(seg_mask, cv2.COLOR_RGB2BGR)

        orig_test = cv2.resize(oirg_img, (args.input_height, args.input_width)) / 127.5 - 1
        orig_test = np.tile(orig_test[np.newaxis, ...], [args.batch_size, 1, 1, 1])
        orig_test = orig_test.astype(np.float32)

        orig_w, orig_h = oirg_img.shape[0], oirg_img.shape[1]
        test_img, mask = erase_img(args, oirg_img, seg_mask)
        test_img = test_img.astype(np.float32)
    ##################saving mask###################

    # os.makedirs(image_save_path, mode=0o777, exist_ok=True)
    # io.imsave('{}.png'.format(image_save_path+'01'), mask)

    # print("mask_shape:{}".format(mask.shape))
        res_img = sess.run(model.test_res_imgs, feed_dict={model.single_orig: orig_test,
                                                        model.single_test: test_img,
                                                        model.single_mask: mask})



        orig = cv2.resize((orig_test[0] + 1), (int(orig_h), int(orig_w)))

        recon = cv2.resize((res_img[0] + 1), (int(orig_h), int(orig_w)))

        # orig = cv2.cvtColor(orig, cv2.COLOR_BGR2RGB)
        #
        # recon = cv2.cvtColor(recon, cv2.COLOR_BGR2RGB)


        single_mse=tf.reduce_mean(tf.nn.l2_loss(orig - recon))
        print("MSE_Single:{}".format(single_mse.eval()))
        total_mse+=single_mse
    ####MSE####
    mse=total_mse/mask_cnt
    print("MSE:{}".format(mse.eval()))
    print("Done.")


def main(_):
    run_config = tf.ConfigProto()
    run_config.gpu_options.allow_growth = True

    with tf.Session(config=run_config) as sess:
        model = network(args)
        mask_list, mask_cnt, orig_list, orig_cnt=get_lists(orig_path, Seg_mask_path, Subjects_UV, Tasks_UV)
        print('Start Testing...')
        test(args, sess, model,orig_list,orig_cnt,mask_list,mask_cnt)


main(args)
