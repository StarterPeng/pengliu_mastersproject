"""
Title: Image-segmentation-keras
Author: Divam Gupta
Date: 2019
Code version: 0.3.0
Availability: https://github.com/divamgupta/image-segmentation-keras

"""
import itertools
import random
import json
from keras.models import *
from keras.layers import *
import cv2
import numpy as np
from types import MethodType
import keras
import os
import glob
import six
from keras.callbacks import TensorBoard,ModelCheckpoint,EarlyStopping

IMAGE_ORDERING="channels_last"
class_colors = [(), ()]
class_colors[0] = 0, 0, 0
class_colors[1] = 255, 255, 255
DATA_LOADER_SEED = 0
random.seed(DATA_LOADER_SEED)


try:
    from tqdm import tqdm
except ImportError:
    print("tqdm not found, disabling progress bars")
    def tqdm(iter):
        return iter

DATA_LOADER_SEED = 0
random.seed(0)
class DataLoaderError(Exception):
    pass

def get_image_array(image_input, width=64, height=64, imgNorm="sub_mean",
                  ordering='channels_first'):
    """ Load image array from input """
    # img = cv2.imread(image_input,1)
    img=image_input
    if imgNorm == None:
        img=img
####################################

    if imgNorm == "sub_and_divide":
        img = np.float32(cv2.resize(img, (width, height))) / 127.5 - 1
    elif imgNorm == "sub_mean":
        img = cv2.resize(img, (width, height))
        img = img.astype(np.float32)
        img[:, :, 0] -= 103.939
        img[:, :, 1] -= 116.779
        img[:, :, 2] -= 123.68
        img = img[:, :, ::-1]
    elif imgNorm == "divide":
        img = cv2.resize(img, (width, height))
        img = img.astype(np.float32)
        img = img/255.0

    if ordering == 'channels_first':
        img = np.rollaxis(img, 2, 0)
    return img


def get_pairs_from_paths(images_path, segs_path):
	Subjects_UV = [
		'F001',
		# 'F002',
		# 'F003',
		# 'F004',
		# 'F005',
		# 'F006',
		# 'F007',
		# 'F008',
		# 'F009',
		# 'F010',
		# 'F011',
		# 'F012',
		# 'F013',
		# 'F014',
		# 'M001',
		# 'M002',
		# 'M003',
		# 'M004',
		# 'M005',
		# 'M006',
		# 'M007',
		# 'M008',
		# 'M009',
		# 'M010',
		# 'M011',
		# 'M012',
		# 'M013',
		# 'M014',

	]
	Tasks_UV = [
		"T1",
		# "T2",
		# "T3",
		# "T4",
		# "T5",
		# "T6",
		# "T7",
		# "T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	text_save('UVlist.txt', UVfilesList)
	text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret
# def get_pairs_from_paths(images_path, segs_path, ignore_non_matching=False):
#     """ Find all the images from the images_path directory and
#         the segmentation images from the segs_path directory
#         while checking integrity of data """
#
#     ACCEPTABLE_IMAGE_FORMATS = [".jpg", ".jpeg", ".png"]
#     ACCEPTABLE_SEGMENTATION_FORMATS = [".png"]
#
#     image_files = []
#     segmentation_files = {}
#
#     for dir_entry in os.listdir(images_path):
#         if os.path.isfile(os.path.join(images_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_IMAGE_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             image_files.append((file_name, file_extension, os.path.join(images_path, dir_entry)))
#
#     for dir_entry in os.listdir(segs_path):
#         if os.path.isfile(os.path.join(segs_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_SEGMENTATION_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             if file_name in segmentation_files:
#                 raise DataLoaderError("Segmentation file with filename {0} already exists and is ambiguous to resolve with path {1}. Please remove or rename the latter.".format(file_name, os.path.join(segs_path, dir_entry)))
#             segmentation_files[file_name] = (file_extension, os.path.join(segs_path, dir_entry))
#
#     return_value = []
#     # Match the images and segmentations
#     for image_file, _, image_full_path in image_files:
#         if image_file in segmentation_files:
#             return_value.append((image_full_path, segmentation_files[image_file][1]))
#         elif ignore_non_matching:
#             continue
#         else:
#             # Error out
#             raise DataLoaderError("No corresponding segmentation found for image {0}.".format(image_full_path))
#
#     return return_value


def get_pairs_from_paths_val_set(images_path, segs_path):
	Subjects_UV = [
		'F015',
		# 'F016',
		# 'F017',
		# 'F018',
		# 'F019',
		# 'F020',
		# 'F021',
		# 'F022',
		# 'M015',
		# 'M016',
		# 'M017',


	]
	Tasks_UV = [
		"T1",
		# "T2",
		# "T3",
		# "T4",
		# "T5",
		# "T6",
		# "T7",
		# "T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	text_save('UVlist.txt', UVfilesList)
	text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret
# def get_pairs_from_paths(images_path, segs_path, ignore_non_matching=False):
#     """ Find all the images from the images_path directory and
#         the segmentation images from the segs_path directory
#         while checking integrity of data """
#
#     ACCEPTABLE_IMAGE_FORMATS = [".jpg", ".jpeg", ".png"]
#     ACCEPTABLE_SEGMENTATION_FORMATS = [".png"]
#
#     image_files = []
#     segmentation_files = {}
#
#     for dir_entry in os.listdir(images_path):
#         if os.path.isfile(os.path.join(images_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_IMAGE_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             image_files.append((file_name, file_extension, os.path.join(images_path, dir_entry)))
#
#     for dir_entry in os.listdir(segs_path):
#         if os.path.isfile(os.path.join(segs_path, dir_entry)) and \
#                 os.path.splitext(dir_entry)[1] in ACCEPTABLE_SEGMENTATION_FORMATS:
#             file_name, file_extension = os.path.splitext(dir_entry)
#             if file_name in segmentation_files:
#                 raise DataLoaderError("Segmentation file with filename {0} already exists and is ambiguous to resolve with path {1}. Please remove or rename the latter.".format(file_name, os.path.join(segs_path, dir_entry)))
#             segmentation_files[file_name] = (file_extension, os.path.join(segs_path, dir_entry))
#
#     return_value = []
#     # Match the images and segmentations
#     for image_file, _, image_full_path in image_files:
#         if image_file in segmentation_files:
#             return_value.append((image_full_path, segmentation_files[image_file][1]))
#         elif ignore_non_matching:
#             continue
#         else:
#             # Error out
#             raise DataLoaderError("No corresponding segmentation found for image {0}.".format(image_full_path))
#
#     return return_value


def get_segmentation_array(image_input, nClasses, width, height, no_reshape=False):
    """ Load segmentation array from input """

    seg_labels = np.zeros((height, width, nClasses))

    if type(image_input) is np.ndarray:
        # It is already an array, use it as it is
        img = image_input
    elif isinstance(image_input, six.string_types) :
        if not os.path.isfile(image_input):
            raise DataLoaderError("get_segmentation_array: path {0} doesn't exist".format(image_input))
        img = cv2.imread(image_input, 1)
    else:
        raise DataLoaderError("get_segmentation_array: Can't process input type {0}".format(str(type(image_input))))

    img = cv2.resize(img, (width, height), interpolation=cv2.INTER_NEAREST)
    img = img[:, :, 0]

    for c in range(nClasses):
        seg_labels[:, :, c] = (img == c).astype(int)

    if not no_reshape:
        seg_labels = np.reshape(seg_labels, (width*height, nClasses))

    return seg_labels



def verify_segmentation_dataset(images_path, segs_path, n_classes, show_all_errors=False):
    try:
        img_seg_pairs = get_pairs_from_paths(images_path, segs_path)
        if not len(img_seg_pairs):
            print("Couldn't load any data from images_path: {0} and segmentations path: {1}".format(images_path, segs_path))
            return False

        return_value = True
        for im_fn, seg_fn in tqdm(img_seg_pairs):
            img = cv2.imread(im_fn)
            seg = cv2.imread(seg_fn)
            # Check dimensions match
            if not img.shape == seg.shape:
                return_value = False
                print("The size of image {0} and its segmentation {1} doesn't match (possibly the files are corrupt).".format(im_fn, seg_fn))
                if not show_all_errors:
                    break
            else:
                max_pixel_value = np.max(seg[:, :, 0])
                if max_pixel_value >= n_classes:
                    return_value = False
                    print("The pixel values of the segmentation image {0} violating range [0, {1}]. Found maximum pixel value {2}".format(seg_fn, str(n_classes - 1), max_pixel_value))
                    if not show_all_errors:
                        break
        if return_value:
            print("Dataset verified! ")
        else:
            print("Dataset not verified!")
        return return_value
    except DataLoaderError as e:
        print("Found error during data loading\n{0}".format(str(e)))
        return False



def verify_segmentation_dataset_val_set(images_path, segs_path, n_classes, show_all_errors=False):
    try:
        img_seg_pairs = get_pairs_from_paths_val_set(images_path, segs_path)
        if not len(img_seg_pairs):
            print("Couldn't load any data from images_path: {0} and segmentations path: {1}".format(images_path, segs_path))
            return False

        return_value = True
        for im_fn, seg_fn in tqdm(img_seg_pairs):
            img = cv2.imread(im_fn)
            seg = cv2.imread(seg_fn)
            # Check dimensions match
            if not img.shape == seg.shape:
                return_value = False
                print("The size of image {0} and its segmentation {1} doesn't match (possibly the files are corrupt).".format(im_fn, seg_fn))
                if not show_all_errors:
                    break
            else:
                max_pixel_value = np.max(seg[:, :, 0])
                if max_pixel_value >= n_classes:
                    return_value = False
                    print("The pixel values of the segmentation image {0} violating range [0, {1}]. Found maximum pixel value {2}".format(seg_fn, str(n_classes - 1), max_pixel_value))
                    if not show_all_errors:
                        break
        if return_value:
            print("Dataset verified! ")
        else:
            print("Dataset not verified!")
        return return_value
    except DataLoaderError as e:
        print("Found error during data loading\n{0}".format(str(e)))
        return False



def image_segmentation_generator(images_path, segs_path, batch_size,
                                 n_classes, input_height, input_width,
                                 output_height, output_width,
                                 do_augment=False):

    img_seg_pairs = get_pairs_from_paths(images_path, segs_path)
    random.shuffle(img_seg_pairs)
    zipped = itertools.cycle(img_seg_pairs)

    while True:
        X = []
        Y = []
        for _ in range(batch_size):
            im, seg = next(zipped)

            im = cv2.imread(im, 1)
            seg = cv2.imread(seg, 1)

            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield np.array(X), np.array(Y)
def image_segmentation_generator_val_set(images_path, segs_path, batch_size,
                                 n_classes, input_height, input_width,
                                 output_height, output_width,
                                 do_augment=False):

    img_seg_pairs = get_pairs_from_paths_val_set(images_path, segs_path)
    random.shuffle(img_seg_pairs)
    zipped = itertools.cycle(img_seg_pairs)

    while True:
        X = []
        Y = []
        for _ in range(batch_size):
            im, seg = next(zipped)

            im = cv2.imread(im, 1)
            seg = cv2.imread(seg, 1)


            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield np.array(X), np.array(Y)

####################################################
def get_pairs_from_paths_test_set(images_path, segs_path):
	Subjects_UV = [
		'F023',
		'M018',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList

	def text_save(filename, data):
		file = open(filename, 'a')
		for i in range(len(data)):
			s = str(data[i]).replace('[', '').replace(']', '')
			s = s.replace("'", '').replace(',', '') + '\n'
			file.write(s)
		file.close()
		print("Successfully saved")

	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))

	text_save('UVlist.txt', UVfilesList)
	text_save('Masklist.txt', MaskfileList)

	ret = []
	for i in range(len(UVfilesList)):
		ret.append((UVfilesList[i], MaskfileList[i]))
	print("pairs", len(ret))
	return ret

###################################################
def image_segmentation_generator_test_set(images_path, segs_path, batch_size,
                                 n_classes, input_height, input_width,
                                 output_height, output_width,
                                 do_augment=False):

    img_seg_pairs = get_pairs_from_paths_val_set(images_path, segs_path)
    random.shuffle(img_seg_pairs)
    zipped = itertools.cycle(img_seg_pairs)

    while True:
        X = []
        Y = []
        for _ in range(batch_size):
            im, seg = next(zipped)

            im = cv2.imread(im, 1)
            seg = cv2.imread(seg, 1)


            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))
            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield [np.array(X), np.array(Y)]
################################################################



def get_imgs_from_paths_test_set(images_path):
	Subjects_UV = [
		'F023',
		'M018',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]
	UVfilesList = []
	MaskfileList = []

	def UvfilenamesReader(datadir, Subjects_UV, Tasks_UV):
		for subject in Subjects_UV:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)
			for task in Tasks_UV:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for UV in unsorted:
					UVpaths = os.path.join(currenttaskPath, UV)
					UVfilesList.append(UVpaths)
				# return UVfilesList
	UvfilenamesReader(images_path, Subjects_UV, Tasks_UV)
	print("UV_images", len(UVfilesList))
	print("Masks", len(MaskfileList))


	ret = []
	for i in range(len(UVfilesList)):
		ret.append(UVfilesList[i])
	return ret



def get_masks_from_paths_test_set(segs_path):
	Subjects_UV = [
		'F023',
		'M018',

	]
	Tasks_UV = [
		"T1",
		"T2",
		"T3",
		"T4",
		"T5",
		"T6",
		"T7",
		"T8"
	]

	MaskfileList = []

	def MaskfilenamesReader(datadir, Subjects_Mask, Tasks_Mask):
		for subject in Subjects_Mask:
			# Get path to image subject folder
			currentSubjectPath = os.path.join(datadir, subject)

			for task in Tasks_Mask:
				currenttaskPath = os.path.join(currentSubjectPath, task)
				unsorted = os.listdir(currenttaskPath)
				unsorted.sort()
				for mask in unsorted:
					maskpaths = os.path.join(currenttaskPath, mask)
					MaskfileList.append(maskpaths)
				# return MaskfileList
		print("Successfully saved")
	MaskfilenamesReader(segs_path, Subjects_UV, Tasks_UV)
	print("Masks", len(MaskfileList))

	ret = []
	for i in range(len(MaskfileList)):
		ret.append(MaskfileList[i])
	return ret



def image_segmentation_generator_test_set_img(images_path, batch_size,

                                 input_height, input_width,
                                 ):

    imgs = get_imgs_from_paths_test_set(images_path)
    zipped = itertools.cycle(imgs)

    while True:
        X = []
        for _ in range(batch_size):
            im = next(zipped)

            im = cv2.imread(im, 1)
            X.append(get_image_array(im, input_width,
                                   input_height, ordering=IMAGE_ORDERING))


        yield np.array(X)
def image_segmentation_generator_test_set_mask(segs_path, batch_size,
                                 n_classes,
                                 output_height, output_width,
                                 do_augment=False):

    segs = get_masks_from_paths_test_set(segs_path)
    zipped = itertools.cycle(segs)

    while True:
        Y = []
        for _ in range(batch_size):
            seg = next(zipped)

            seg = cv2.imread(seg, 1)


            Y.append(get_segmentation_array(
                seg, n_classes, output_width, output_height))

        yield np.array(Y)
def train(model,
          train_images,
          train_annotations,
          input_height=None,
          input_width=None,
          n_classes=None,
          verify_dataset=False,
          checkpoints_path=None,
          logdir=None,
          epochs=5,
          batch_size=2,
          validate=False,
          val_images=None,
          val_annotations=None,
          val_batch_size=2,
          validation_steps=200,
          auto_resume_checkpoint=False,
          load_weights=None,
          steps_per_epoch=512,
          optimizer_name='adadelta',

          test=False,
          test_images=None,
          test_annotations=None,
          test_batch_size=50,
          test_step=200
          ):
    # check if user gives model name instead of the model object
    if isinstance(model, six.string_types):
        # create the model from the name
        assert (n_classes is not None), "Please provide the n_classes"
        if (input_height is not None) and (input_width is not None):
            model = model_from_name[model](
                n_classes, input_height=input_height, input_width=input_width)
        else:
            model = model_from_name[model](n_classes)

    n_classes = model.n_classes
    input_height = model.input_height
    input_width = model.input_width
    output_height = model.output_height
    output_width = model.output_width

    if validate:
        assert val_images is not None
        assert val_annotations is not None

    if optimizer_name is not None:
        if optimizer_name=='sgd':
            model.compile(loss='binary_crossentropy',
                          optimizer=optimizer_name,
                          metrics=['accuracy'])
        if optimizer_name=='Adam':
            model.compile(loss='binary_crossentropy',
                          optimizer=keras.optimizers.Adam(lr=0.001),
                          metrics=['accuracy'])
    else:
        model.compile(loss='categorical_crossentropy',
                      optimizer=optimizer_name,
                      metrics=['accuracy'])


    if checkpoints_path is not None:
        with open(checkpoints_path+"_config.json", "w") as f:
            json.dump({
                "model_class": model.model_name,
                "n_classes": n_classes,
                "input_height": input_height,
                "input_width": input_width,
                "output_height": output_height,
                "output_width": output_width
            }, f)

    if load_weights is not None and len(load_weights) > 0:
        print("Loading weights from ", load_weights)
        model.load_weights(load_weights)

    if auto_resume_checkpoint and (checkpoints_path is not None):
        latest_checkpoint = find_latest_checkpoint(checkpoints_path)
        if latest_checkpoint is not None:
            print("Loading the weights from latest checkpoint ",
                  latest_checkpoint)
            model.load_weights(latest_checkpoint)

    # if verify_dataset:
    #     print("Verifying training dataset")
    #     verified = verify_segmentation_dataset(train_images, train_annotations, n_classes)
    #     print("Training set verified")
    #     # assert verified
    #     if validate:
    #         print("Verifying validation dataset")
    #         verified = verify_segmentation_dataset_val_set(val_images, val_annotations, n_classes)
    #         print("Training set verified")
    #         # assert verified


    train_gen = image_segmentation_generator(
        train_images, train_annotations,  batch_size,  n_classes,
        input_height, input_width, output_height, output_width)

    if validate:
        val_gen = image_segmentation_generator_val_set(
            val_images, val_annotations,  val_batch_size,
            n_classes, input_height, input_width, output_height, output_width)

    if test:
        test_pairs_gen = image_segmentation_generator_test_set(
            test_images, test_annotations, test_batch_size,
            n_classes, input_height, input_width, output_height, output_width)
    if not validate:
        model.fit_generator(train_gen,
                            steps_per_epoch,
                            epochs=epochs,
                            callbacks=[TensorBoard(log_dir='logs/unetNov15')],
                            workers=1)
        if checkpoints_path is not None:
            model.save_weights(checkpoints_path + "." + str(epochs))
            print("saved ", checkpoints_path + ".model." + str(epochs))
        print("Finished Epoch", epochs)
    else:


        early_stop = EarlyStopping(monitor='val_loss',
                                   min_delta=0.00003,
                                   patience=1,
                                   mode='min',
                                   )
        model.fit_generator(train_gen,
                            steps_per_epoch,
                            validation_data=val_gen,
                            validation_steps=validation_steps,
                            epochs=epochs,
                            callbacks = [TensorBoard(log_dir=logdir,
                                                     histogram_freq=0,
                                                     write_graph=True,
                                                     write_images=1
                                                     ),
                                         early_stop,
                                           ],
                            workers = 1)
        #####################################tensorboard###########################

    if test:
        testloss, testacc = model.evaluate(x=test_pairs_gen,
                                    # batch_size=test_batch_size,
                                    steps=test_step,
                                    verbose=1)
        print('Test loss:', testloss)
        print('Test accuracy:', testacc)
        ###########################################################################

        #########save best model
        if checkpoints_path is not None:
            model.save_weights(checkpoints_path + "." + str(epochs))
            print("saved ", checkpoints_path + ".model." + str(epochs))
        print("Finished Epoch", epochs)


def get_segmentation_model_my_unet(input, output):

    img_input = input
    o = output

    o_shape = Model(img_input, o).output_shape
    i_shape = Model(img_input, o).input_shape

    if IMAGE_ORDERING == 'channels_first':
        output_height = o_shape[2]
        output_width = o_shape[3]
        input_height = i_shape[2]
        input_width = i_shape[3]
        n_classes = o_shape[1]
        o = (Reshape((-1, output_height*output_width)))(o)
        o = (Permute((2, 1)))(o)
    elif IMAGE_ORDERING == 'channels_last':
        output_height = o_shape[1]
        output_width = o_shape[2]
        input_height = i_shape[1]
        input_width = i_shape[2]
        n_classes = o_shape[3]
        o = (Reshape((output_height*output_width, -1)))(o)

    o = (Activation('softmax'))(o)
    model = Model(img_input, o)
    model.output_width = output_width
    model.output_height = output_height
    model.n_classes = n_classes
    model.input_height = input_height
    model.input_width = input_width
    model.model_name = ""

    model.train = MethodType(train, model)
    model.predict_segmentation = MethodType(predict, model)
    # model.predict_multiple = MethodType(predict_multiple, model)
    # model.evaluate_segmentation = MethodType(evaluate, model)

    return model

def my_unet(n_classes,input_height=64, input_width=64):
    if IMAGE_ORDERING == 'channels_first':
        img_input = Input(shape=(3, input_height, input_width))
    elif IMAGE_ORDERING == 'channels_last':
        img_input = Input(shape=(input_height, input_width, 3))

    c_layer1= Conv2D(32, (3, 3),
                     activation='relu',
                     padding='same')(img_input)
    c_layer1 = Conv2D(32, (3, 3), activation='relu', padding='same')(c_layer1)
    p_layer1 = MaxPooling2D(pool_size=(2, 2))(c_layer1)

    c_layer2 = Conv2D(64, (3, 3), activation='relu', padding='same')(p_layer1)
    c_layer2 = Conv2D(64, (3, 3), activation='relu', padding='same')(c_layer2)
    p_layer2 = MaxPooling2D(pool_size=(2, 2))(c_layer2)

    c_layer3 = Conv2D(128, (3, 3), activation='relu', padding='same')(p_layer2)
    c_layer3 = Conv2D(128, (3, 3), activation='relu', padding='same')(c_layer3)
    p_layer3 = MaxPooling2D(pool_size=(2, 2))(c_layer3)

    c_layer4 = Conv2D(256, (3, 3), activation='relu', padding='same')(p_layer3)
    c_layer4 = Conv2D(256, (3, 3), activation='relu', padding='same')(c_layer4)
    p_layer4 = MaxPooling2D(pool_size=(2, 2))(c_layer4)

    c_layer5 = Conv2D(512, (3, 3), activation='relu', padding='same')(p_layer4)
    c_layer5 = Conv2D(512, (3, 3), activation='relu', padding='same')(c_layer5)

    up_layer1 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(c_layer5), c_layer4], axis=3)
    c_layer6 = Conv2D(256, (3, 3), activation='relu', padding='same')(up_layer1)
    c_layer6 = Conv2D(256, (3, 3), activation='relu', padding='same')(c_layer6)

    up_layer2 = concatenate([Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c_layer6), c_layer3], axis=3)
    c_layer7 = Conv2D(128, (3, 3), activation='relu', padding='same')(up_layer2)
    c_layer7 = Conv2D(128, (3, 3), activation='relu', padding='same')(c_layer7)

    up_layer3 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c_layer7), c_layer2], axis=3)
    c_layer8 = Conv2D(64, (3, 3), activation='relu', padding='same')(up_layer3)
    c_layer8 = Conv2D(64, (3, 3), activation='relu', padding='same')(c_layer8)

    up_layer4 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c_layer8), c_layer1], axis=3)
    c_layer9 = Conv2D(32, (3, 3), activation='relu', padding='same')(up_layer4)
    c_layer9 = Conv2D(32, (3, 3), activation='relu', padding='same')(c_layer9)


    model = get_segmentation_model_my_unet(img_input, c_layer9)
    model.model_name = "my_unet"
    return model
model_from_name = {}
model_from_name["my_unet"] = my_unet

def find_latest_checkpoint(checkpoints_path, fail_safe=True):

    def get_epoch_number_from_path(path):
        return path.replace(checkpoints_path, "").strip(".")

    # Get all matching files
    all_checkpoint_files = glob.glob(checkpoints_path + ".*")
    # Filter out entries where the epoc_number part is pure number
    all_checkpoint_files = list(filter(lambda f: get_epoch_number_from_path(f).isdigit(), all_checkpoint_files))
    if not len(all_checkpoint_files):
        # The glob list is empty, don't have a checkpoints_path
        if not fail_safe:
            raise ValueError("Checkpoint path {0} invalid".format(checkpoints_path))
        else:
            return None

    # Find the checkpoint file with the maximum epoch
    latest_epoch_checkpoint = max(all_checkpoint_files, key=lambda f: int(get_epoch_number_from_path(f)))
    return latest_epoch_checkpoint

def model_from_checkpoint_path(checkpoints_path):
    assert (os.path.isfile(checkpoints_path + "_config.json")
            ), "Checkpoint not found."
    model_config = json.loads(
        open(checkpoints_path + "_config.json", "r").read())
    latest_weights = find_latest_checkpoint(checkpoints_path)
    assert (latest_weights is not None), "Checkpoint not found."
    model = model_from_name[model_config['model_class']](
        model_config['n_classes'], input_height=model_config['input_height'],
        input_width=model_config['input_width'])
    print("loaded weights ", latest_weights)
    model.load_weights(latest_weights)
    return model


def predict(model=None, inp=None, out_fname=None, checkpoints_path=None):
    if model is None and (not checkpoints_path is None):
        model = model_from_checkpoint_path(checkpoints_path)

    assert (not inp is None)
    assert ((type(inp) is np.ndarray) or isinstance(inp,
                                                    six.string_types)), "Inupt should be the CV image or the input file name"

    if isinstance(inp, six.string_types):
        inp = cv2.imread(inp)

    assert len(inp.shape) == 3, "Image should be h,w,3 "
    orininal_h = inp.shape[0]
    orininal_w = inp.shape[1]

    output_width = model.output_width
    output_height = model.output_height
    input_width = model.input_width
    input_height = model.input_height
    n_classes = model.n_classes

    x = get_image_array(inp, input_width, input_height, ordering=IMAGE_ORDERING, imgNorm='sub_mean')
    pr = model.predict(np.array([x]))[0]

    print("pr predicted:{}".format(pr.shape))
    #########################################pr.shape=(1024, 2)#######################
    w, h = pr.shape

    txt_file_name = 'predictionvalues.txt'
    txt = open(txt_file_name, 'w')
    for i in range(w):
        # for j in range(h):
        txt.write(str(pr[i]))
        txt.write('\n')
    txt.close()
    #########################################



    pr = pr.reshape((output_height, output_width, n_classes)).argmax(axis=2)

    seg_img = np.zeros((output_height, output_width, 3))
    colors = class_colors
    print("colors:{}".format(colors))
    print("n_classes:{}".format(n_classes))
    print("pr:{}".format(pr.shape))

    for c in range(0, 2):
        seg_img[:, :, 0] += ((pr[:, :] == c) * (colors[c][0])).astype('float32')
        seg_img[:, :, 1] += ((pr[:, :] == c) * (colors[c][1])).astype('float32')
        seg_img[:, :, 2] += ((pr[:, :] == c) * (colors[c][2])).astype('float32')

    print("Original size:", orininal_h, orininal_w)
    print("New size:", seg_img.shape)

    seg_img = cv2.resize(seg_img, (orininal_w, orininal_h))
    seg_img = (seg_img > 0) * 255.0
    print("seg_img:{}".format(seg_img))
    if not out_fname is None:
        cv2.imwrite(out_fname, seg_img)
#############################################################################################
#############################################################################################


    # return pr,seg_img


img = cv2.imread('2440.png', 1)
predict(checkpoints_path='/mnt/Core/BestModelNov19th/MyUnetNov19th',out_fname='2440holeout.png',inp='2440.png')